package com.binga.designpatterns.singleton;

/**
 * @Description: 静态内部类单例模式
 * @Author: binga
 * @Date: 2020/8/20 17:25
 * @Blog: https://blog.csdn.net/pang5356
 */
public class StaticInnerClassSingleton {

    private StaticInnerClassSingleton() {
        if (null != SingletonInstanceHolder.instance) {
            throw new RuntimeException("单例模式不允许创建多个实例");
        }
    }

    private static class SingletonInstanceHolder {
        private static StaticInnerClassSingleton instance = new StaticInnerClassSingleton();
    }

    public static StaticInnerClassSingleton getInstance() {
        return SingletonInstanceHolder.instance;
    }
}
