package com.binga.algorithm.datastructure.stack;

/**
 * @Description: 使用顺序存储结构实现栈，也就是数组实现栈结构。
 *               由于栈是只能在一端进行操作的线程表，所以可以将数组的
 *               起始作为栈的栈底，而另一端作为栈的栈顶，那么元素的
 *               入栈和出栈均在线性表的尾部进行操作，我们可以通过维护一个
 *               top指针用于标识栈的栈顶位置，当有数据压栈时将top加一，
 *               当数据减一时，将top减一，栈为空时，则top为-1。
 * @Author: binga
 * @Date: 2020/12/7 15:16
 * @Blog: https://blog.csdn.net/pang5356
 */
public class SqStack implements Stack {

    // 栈空间最大为20
    private static final int STACK_SIZE = 20;

    // 存储栈中数据元素
    private Object[] table;

    // 记录当前栈顶位置
    private int top;

    public SqStack() {
        this.table = new Object[STACK_SIZE];
        this.top = -1;
    }

    /**
     * 功能描述: 返回栈中元素个数
     * @param: []
     * @return: int
     * @auther: binga
     * @date: 2020/12/7 15:23
     */
    public int size() {
        return this.top + 1;
    }

    /**
     * 功能描述: 将参数elem压入栈中
     * @param: [elem]
     * @return: void
     * @auther: binga
     * @date: 2020/12/7 15:23
     */
    public void push(Object elem) {
        if ( (top + 1) == STACK_SIZE) throw new RuntimeException("stack is full");
        // 先将栈顶标志top加一，然后将元素添加至栈顶位置
        table[++top] = elem;
    }

    /**
     * 功能描述: 将栈顶元素弹出
     * @param: []
     * @return: java.lang.Object
     * @auther: binga
     * @date: 2020/12/7 15:26
     */
    public Object pop() {
        if (top == -1) return null;

        Object elem = table[top];
        table[top--] = null;// help gc
        return elem;
    }

    /**
     * 功能描述: 查看栈中栈顶元素
     * @param: []
     * @return: java.lang.Object
     * @auther: binga
     * @date: 2020/12/7 15:29
     */
    public Object peek() {
        if (top == -1) return null;
        return table[top];
    }
}
