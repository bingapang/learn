package com.binga.designpatterns.fatory.factorymethod;

/**
 * @Description: 绩效薪酬计算器，绩效薪资是通过评定获取，所以职级去绩效表中
 *               通过人员代码查询即可
 * @Author: binga
 * @Date: 2020/8/20 10:10
 * @Blog: https://blog.csdn.net/pang5356
 */
public class PerformanceSalaryCalculator implements SalaryCalculator{

    public void calculate(String userCode) {
        // 查询人员的绩效
        System.out.println("query " + userCode + " performance from database");
        // 保存人员的绩效
        System.out.println("save " + userCode + " performance");
    }
}
