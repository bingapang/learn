package com.binga.algorithm.datastructure.queue;

/**
 * @Description: 使用链表结构实现队列
 *               通过链表实现队列，可以通过定义两个指针head和tail用于代表
 *               队头和队为的引用。head用于标识队列的起始节点，不存储数据。
 *               而tail则指向队列的尾部节点。
 *               我们定义一下的条件:
 *               当队列为空时，满足以下条件
 *                  head == tail
 *               由于链表实现不需指定空间的大小，所以链表实现的队列的大小不需要
 *               指定，但是我们需要通过一个维护一个变量size来记录队列中元素的
 *               个数。
 *
 *
 *
 * @Author: binga
 * @Date: 2020/12/9 15:52
 * @Blog: https://blog.csdn.net/pang5356
 */
public class LinkQueue implements Queue {

    // 头节点
    private QueueNode head;
    // 尾节点
    private QueueNode tail;

    // 记录队列中元素个数
    private int size;

    public int length() {
        return size;
    }

    public LinkQueue() {
        QueueNode node = new QueueNode();
        tail = head = node;
    }

    /**
     * 功能描述: 通过判断头节点与尾节点是否相等，也可通过size进行判断
     * @param: []
     * @return: boolean
     * @auther: binga
     * @date: 2020/12/9 15:59
     */
    public boolean isEmpty() {
        return head == tail;
    }


    public void enqueue(Object elem) {
        QueueNode node = new QueueNode();
        node.data = elem;
        tail.next = node;
        tail = node;

        size++;
    }

    public Object dequeue() {
        if (head == tail) throw new RuntimeException("queue is empty");

        QueueNode p = head.next;
        Object elem = p.data;
        head.next = p.next;

        p.next = null;
        p.data = null; // help gc
        // 当前队列只有一个元素，那么当前元素剔除后，则为空
        if (p == tail) tail = head;

        size--;
        return elem;
    }

    static class QueueNode {
        private Object data;
        private QueueNode next;
    }
}
