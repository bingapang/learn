package com.binga.jvm.jvmtools;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: Jstat工具的使用测试
 *              -Xms1024m -Xmx1024m -Xmn512m
 * @Author: binga
 * @Date: 2020/9/15 10:29
 * @Blog: https://blog.csdn.net/pang5356
 */
public class JstatToolsTest {

    public static void main(String[] args) {
        List<User> users = new ArrayList<>();
        int index = 0;
        while (true) {
            if (index % 10000 == 0) {
                users.add(new User());
            } else {
                new User();
            }
        }
    }

    static class User {

    }
}
