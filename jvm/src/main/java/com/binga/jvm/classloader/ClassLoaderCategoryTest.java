package com.binga.jvm.classloader;

import sun.net.spi.nameservice.dns.DNSNameService;

/**
 * @Description: 演示jvm中类加载器
 * @Author: binga
 * @Date: 2020/8/18 16:45
 * @Blog: https://blog.csdn.net/pang5356
 */
public class ClassLoaderCategoryTest {

    public static void main(String[] strings) {
        System.out.println(Object.class.getClassLoader());
        // java提供的与DNS服务交互的api(位于{JAVA_HOME}/jre//lib/ext/dnsns.jar包中)
        System.out.println(DNSNameService.class.getClassLoader());
        System.out.println(ClassLoaderCategoryTest.class.getClassLoader());
        /**
         * 运行结果
         * null
         * sun.misc.Launcher$ExtClassLoader@6d6f6e28
         * sun.misc.Launcher$AppClassLoader@58644d46
         *
         */
    }
}
