package com.binga.designpatterns.singleton;

import java.io.ObjectStreamException;

/**
 * @Description: 懒汉式单例模式
 * @Author: binga
 * @Date: 2020/8/20 17:07
 * @Blog: https://blog.csdn.net/pang5356
 */
public class LazySingleton {

    private static volatile LazySingleton instance;

    private LazySingleton() {
        if (null != instance) {
            throw new RuntimeException("单例模式不允许创建多个实例");
        }
    }

    /**
     * 功能描述:
     *          这里说明一下使用局部变量的作用，局部变量在单例初始化后应对并发提升
     *          性能是很大的。在单例实例已经被初始化，通过使用局部变量，保证了整个方法
     *          只读取了一次instance的值，如果不使用局部变量，那么对instance的读取就
     *          会发生两次，如下：
     *          public static LazySingleton getInstance() {
     *              // 第一次读取
     *              if(null == instance) {
     *                  synchronized (LazySingleton.class) {
     *                      if (null == instance) {
     *                          instance =  new LazySingleton();
     *                      }
     *                  }
     *              }
     *              // 第二次读取
     *              return instance;
     *          }
     * @param:
     * @return: com.binga.designpatterns.singleton.LazySingleton
     * @auther: binga
     * @date: 2020/8/20 17:19
     */
    public static LazySingleton getInstance() {
        LazySingleton lazySingleton = instance;
        if (null == lazySingleton) {
            synchronized (LazySingleton.class) {
                lazySingleton = instance;
                if (null == lazySingleton) {
                    instance = lazySingleton = new LazySingleton();
                }
            }
        }
        return lazySingleton;
    }

    public Object readResolve() throws ObjectStreamException {
        return LazySingleton.instance;
    }
}
