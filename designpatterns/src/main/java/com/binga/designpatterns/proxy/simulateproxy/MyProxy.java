package com.binga.designpatterns.proxy.simulateproxy;

import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @Description: 模拟JDK中的Proxy类
 *               生成代理类主要分为三步骤
 *               1>通过需要指定的代理接口，从而生成对应的实现类代码;
 *               2>使用JavaCompiler对生成的代码进行编译，生成Class文件;
 *               3>生成class文件后，通过AppClassLoader加载class文件初始化，然后实例化就是代理对象了。
 * @Author: binga
 * @Date: 2020/8/25 11:02
 * @Blog: https://blog.csdn.net/pang5356
 */
public class MyProxy {
    
    /**
     * 功能描述: 定义的钩子属性，InvocationHandler的 invoke方法在代理对象调用方法时会调用
     * @param: 
     * @return: 
     * @auther: binga
     * @date: 2020/8/25 18:05
     */
    protected InvocationHandler h;
    
    /**
     * 功能描述: 
     * @param: 不允许实例化
     * @return: 
     * @auther: binga
     * @date: 2020/8/25 18:07
     */
    private MyProxy() {

    }

    /**
     * 功能描述: 定义有参的构造函数，用于添加InvocationHandler的添加
     * @param: [h]
     * @return:
     * @auther: binga
     * @date: 2020/8/26 10:02
     */
    protected MyProxy(InvocationHandler h) {
        this.h = h;
    }
    
    /**
     * 功能描述: 该方法就是通过生成代码、编译、加载及创建实例从而动态的生成代理类的
     * @param: [classLoader, interfaces, h]
     * @return: java.lang.Object
     * @auther: binga
     * @date: 2020/8/25 18:07
     */
    public static Object getProxyInstance(ClassLoader classLoader,Class<?>[] interfaces, InvocationHandler h) {
        // 校验
        if (null == interfaces || interfaces.length ==0) {
            throw new RuntimeException("interfaces can not be null");
        }
        // 1.生成代码
        String code = generateCode(interfaces);
        // 2.保存代码
        File file = saveCode(code);
        // 3.编译文件，生成class文件
        compileCode(file);
        // 4. 通过反射生产代理对象
        Object proxyObj = generateProxyObject(classLoader, h);
        return proxyObj;
    }

    /**
     * 功能描述: 在加载完代理类的字节码文件后通过反射获取代理对象实例
     * @param: [classLoader, h]
     * @return: java.lang.Object
     * @auther: binga
     * @date: 2020/8/25 18:08
     */
    private static Object generateProxyObject(ClassLoader classLoader, InvocationHandler h) {
        Object object = null;
        try {
            String clazzName = MyProxy.class.getPackage().getName() + "." + "$Proxy0";
            Class<?> aClass = classLoader.loadClass(clazzName);
            Constructor<?> declaredConstructor = aClass.getDeclaredConstructor(new Class[]{InvocationHandler.class});
            object = declaredConstructor.newInstance(h);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return object;
    }

    /**
     * 功能描述: 保存动态生成的代码，保存至当前类编译后的字节码路径下，以便于编译时
     *          能够将class字节码文件输出的指定的路径下，从而可以是AppClassLoader扫描
     *          到
     * @param: [code]
     * @return: java.io.File
     * @auther: binga
     * @date: 2020/8/25 18:09
     */
    private static File saveCode(String code) {
        String path = MyProxy.class.getResource("").getPath();
        File file = new File(path, "$Proxy0.java");
        try(FileOutputStream out = new FileOutputStream(file)) {
            out.write(code.getBytes());
            out.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    /**
     * 功能描述: 编译动态生成的代码生成字节码文件
     * @param: [file]
     * @return: void
     * @auther: binga
     * @date: 2020/8/25 18:11
     */
    public static void compileCode(File file) {
        try {
            // 获取编译器
            JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
            StandardJavaFileManager manager = compiler.getStandardFileManager(null, null, null);
            Iterable<? extends JavaFileObject> javaFileObjects = manager.getJavaFileObjects(file);

            JavaCompiler.CompilationTask task = compiler.getTask(null, manager, null, null, null, javaFileObjects);
            task.call();
            manager.close();
            System.out.println("compile finish");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 功能描述: 根据指定的接口生成MyProxy的子类代码，即.java文件
     * @param: [interfaces]
     * @return: java.lang.String
     * @auther: binga
     * @date: 2020/8/25 18:11
     */
    private static String generateCode(Class<?>[] interfaces) {
        StringBuilder code = new StringBuilder();
        // 声明包
        code.append("package com.binga.designpatterns.proxy.simulateproxy; \r\n");

        // 导入
        for (Class<?> anInterface: interfaces) {
            code.append("import " + anInterface.getName() + "; \r\n");
        }

        // 声明类
        code.append("public class $Proxy0 extends com.binga.designpatterns.proxy.simulateproxy.MyProxy implements ");
        for (int i = 0; i < interfaces.length; i++) {
            code.append(interfaces[i].getName());
            if (i != interfaces.length -1) {
                code.append(",");
            }
        }
        code.append(" { \r\n");

        // equal
        code.append("   private static java.lang.reflect.Method m0; \r\n");
        // toString
        code.append("   private static java.lang.reflect.Method m1; \r\n");
        // hashCode
        code.append("   private static java.lang.reflect.Method m2; \r\n");

        int index = 3;
        for (int i = 0; i < interfaces.length; i++) {
            Class<?> anInterface = interfaces[i];
            Method[] declaredMethods = anInterface.getDeclaredMethods();
            for (int j = 0; j < declaredMethods.length; j++) {
                code.append("   private static java.lang.reflect.Method m" + index++ + "; \r\n");
            }
        }

        // 构造
        code.append("   public $Proxy0(java.lang.reflect.InvocationHandler h) { \r\n");
        code.append("       super(h); \r\n");
        code.append("   } \r\n");

        //equals方法
        code.append("   @Override\r\n");
        code.append("   public final boolean equals(java.lang.Object paramObject) { \r\n");
        code.append("       try { \r\n");
        code.append("           return ((Boolean)this.h.invoke(this, m0, new Object[] { paramObject })).booleanValue(); \r\n");
        code.append("       } catch(Throwable e) { \r\n");
        code.append("           e.printStackTrace(); \r\n");
        code.append("       }\r\n");
        code.append("       return false;\r\n");
        code.append("   }\r\n");

        // toString方法
        code.append("   @Override\r\n");
        code.append("   public java.lang.String toString() { \r\n");
        code.append("       try { \r\n");
        code.append("           return ((String)this.h.invoke(this, m1, null));\r\n");
        code.append("       } catch(Throwable e) { \r\n");
        code.append("           e.printStackTrace(); \r\n");
        code.append("       }\r\n");
        code.append("       return null;\r\n");
        code.append("   }\r\n");

        //hashCode 方法
        code.append("   @Override\r\n");
        code.append("   public int hashCode() {\r\n");
        code.append("       try { \r\n");
        code.append("           return ((Integer)this.h.invoke(this, m2, null)).intValue();\r\n");
        code.append("       } catch(Throwable e) { \r\n");
        code.append("           e.printStackTrace(); \r\n");
        code.append("       }\r\n");
        code.append("       return 0;\r\n");
        code.append("   }\r\n");

        int methodIndex = 3;
        for (int i = 0; i < interfaces.length; i++) {
            Class<?> anInterface = interfaces[i];
            Method[] declaredMethods = anInterface.getDeclaredMethods();
            for (int j = 0; j < declaredMethods.length; j++) {
                Method declaredMethod = declaredMethods[j];
                Class<?> returnType = declaredMethod.getReturnType();
                String returnStr = getReturnStr(returnType);
                code.append("   public " + returnStr + " " + declaredMethod.getName() + "( ");
                Class<?>[] parameterTypes = declaredMethod.getParameterTypes();
                for (int k = 0; k < parameterTypes.length; k++) {
                    String paramStr = getReturnStr(parameterTypes[k]);
                    code.append(paramStr + "var" + k);
                    if (parameterTypes.length - 1 != k)
                        code.append(", ");
                }
                code.append(") { \r\n");
                code.append("       try { \r\n");
                code.append("           java.lang.Object[] args = {");
                for (int k = 0; k < parameterTypes.length; k++) {
                    code.append("var" + k);
                    if (parameterTypes.length != 1) {
                        code.append(",");
                    }
                }
                code.append("}; \r\n");
                if ("void".equals(returnStr)) {
                    code.append("       this.h.invoke(this, m" + methodIndex++  + ", null); \r\n");
                    code.append("       return;\r\n");
                } else {
                    if(isPrimitive(returnType)) {
                        String primitiveCast = getPrimitiveCast(returnType);
                        String primitive = getReturnStr(returnType) + "Value()";
                        code.append("       return ((" + primitiveCast + ") this.h.invoke(this, m" + methodIndex++ + ", args))." + primitive +"; \r\n");
                    } else {
                        code.append("       return (" + returnStr + ") this.h.invoke(this, m" + methodIndex++ + ", args); \r\n");
                    }
                }

                code.append("       } catch(Throwable e) { \r\n");
                code.append("           e.printStackTrace(); \r\n");
                code.append("       }\r\n");
                code.append("   }\r\n");
            }
        }

        // 静态代码块
        code.append("   static { \r\n");
        code.append("       try{ \r\n");
        code.append("           m0 = Class.forName(\"java.lang.Object\").getMethod(\"equals\", new Class[] { Class.forName(\"java.lang.Object\") });\r\n");
        code.append("           m1 = Class.forName(\"java.lang.Object\").getMethod(\"toString\", new Class[0]);\r\n");
        code.append("           m2 = Class.forName(\"java.lang.Object\").getMethod(\"hashCode\", new Class[0]);\r\n");
        int staticIndex = 3;
        for (int i = 0; i < interfaces.length; i++) {
            Class<?> anInterface = interfaces[i];
            String interfaceName = anInterface.getName();
            Method[] declaredMethods = anInterface.getDeclaredMethods();
            for (int j = 0; j < declaredMethods.length; j++) {
                Method declaredMethod = declaredMethods[j];
                String methodName = declaredMethod.getName();
                code.append("           m" + staticIndex++ + " = Class.forName(\"" + interfaceName + "\").getMethod(\"" + methodName + "\",");
                if (declaredMethod.getParameterTypes().length == 0) {
                    code.append("new Class[0]");
                } else {
                    Class<?>[] parameterTypes = declaredMethod.getParameterTypes();
                    code.append("new Class[]{");
                    for (int k = 0; k < parameterTypes.length; k++) {
                        if(isPrimitive(parameterTypes[k])) {
                            String paraStr = getReturnStr(parameterTypes[k]) + ".class";
                            code.append(paraStr);
                        } else {
                            code.append("Class.forName(\"" + getReturnStr(parameterTypes[k]) + "\")");
                        }

                        if (k != parameterTypes.length -1) {
                            code.append(",");
                        }
                    }
                    code.append("}");
                }

                code.append("); \r\n");
                // 组装param

            }
        }
        code.append("       } catch(Throwable e) { \r\n ");
        code.append("           e.printStackTrace(); \r\n");
        code.append("       }\r\n");
        code.append("   }\r\n");

        code.append("}");

        return code.toString();
    }


    /**
     * 功能描述: 根据基本类型，返回其封装类的名称
     * @param: [returnType]
     * @return: java.lang.String
     * @auther: binga
     * @date: 2020/8/26 10:03
     */
    private static String getPrimitiveCast(Class<?> returnType) {
        if (byte.class == returnType)
            return "Byte";
        if (short.class == returnType)
            return "Short";
        if (char.class == returnType)
            return "Character";
        if (int.class == returnType)
            return "Integer";
        if (boolean.class == returnType)
            return "Boolean";
        if (float.class == returnType)
            return "Float";
        if (double.class == returnType)
            return "Double";
        if (long.class == returnType)
            return "Long";
        return "";
    }

    /**
     * 功能描述: 判断是否是基础数据
     * @param: [returnType]
     * @return: boolean
     * @auther: binga
     * @date: 2020/8/26 10:04
     */
    private static boolean isPrimitive(Class<?> returnType) {
        if (byte.class == returnType || short.class == returnType
                || char.class == returnType || int.class == returnType
                || boolean.class == returnType || float.class == returnType
                || double.class == returnType || long.class == returnType) {
            return true;
        }
        return false;
    }

    /**
     * 功能描述: 组装返回类型的字符串信息
     * @param: [returnType]
     * @return: java.lang.String
     * @auther: binga
     * @date: 2020/8/26 10:04
     */
    private static String getReturnStr(Class<?> returnType) {
        //无返回类型
        if (void.class == returnType)
            return "void";

        // 基础数据类型
        if (byte.class == returnType)
            return "byte";
        if (short.class == returnType)
            return "short";
        if (char.class == returnType)
            return "char";
        if (int.class == returnType)
            return "int";
        if (boolean.class == returnType)
            return "boolean";
        if (float.class == returnType)
            return "float";
        if (double.class == returnType)
            return "double";
        if (long.class == returnType)
            return "long";
        return returnType.getName();
    }
}
