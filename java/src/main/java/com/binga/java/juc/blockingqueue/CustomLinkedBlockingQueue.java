package com.binga.java.juc.blockingqueue;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Description: 链表实现阻塞队列，并且为有界的队列
 * @Author: binga
 * @Date: 2020/11/5 15:45
 * @Blog: https://blog.csdn.net/pang5356
 */
public class CustomLinkedBlockingQueue<T> implements CustomBlockingQueue<T> {

    // 容量
    private int capacity;
    // 记录队列中的数量
    private AtomicInteger count = new AtomicInteger(0);

    // 由于使用链表实现队列时，当队列中存在数据时则取和放是不需要获取同一把锁的
    // 所以这里有两把锁。一把用于放元素的所有线程序列化访问，一把
    // 用于取元素所有线程序列化访问
    // 取锁
    private ReentrantLock takeLock;
    // 队列中没有元素了，使用该条件队列阻塞
    private Condition notEmpty;

    // 放锁
    private ReentrantLock putLock;
    // 队列中元素满了，使用该条件队列阻塞
    private Condition notFull;

    // 用于记录头节点和尾节点，头节点不存储元素
    private Node first;
    private Node last;


    // 链表节点类
    static class Node {
        Object item;
        Node next;

        Node(Object item) {
            this.item = item;
        }
    }

    public CustomLinkedBlockingQueue() {
        // 默认非公平
        this(Integer.MAX_VALUE, false);
    }

    public CustomLinkedBlockingQueue(int capacity) {
        this(capacity,false);
    }


    public CustomLinkedBlockingQueue(int capacity, boolean fair) {
        if (capacity <= 0) {
            throw new IllegalArgumentException("capacity can not be 0 or negative");
        }

        this.capacity = capacity;

        // 锁相关的初始化
        this.takeLock = new ReentrantLock(fair);
        this.notEmpty = takeLock.newCondition();

        this.putLock = new ReentrantLock(fair);
        this.notFull = putLock.newCondition();

        // 链表节点初始化
        first = last = new Node(null);
    }


    @Override
    public T take() {
        T t = null;

        ReentrantLock takeLock = this.takeLock;
        // 先获取锁
        takeLock.lock();
        int a = -1;

        try {
            // 没有元素的话就一直循环等待
            while (count.get() == 0) {
                notEmpty.await();
            }

            // 有元素了，获取
            t = dequeue();
            // 将元素数量减1
            a = count.getAndDecrement();
            // 如果在当前线程获取元素时队列中不止1个，则唤醒其他的获取元素的线程
            if (a > 1) {
                notEmpty.signal();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            takeLock.unlock();
        }
        // 加入在当前线程获取元素前队列已经满了，现在拿走一个，那么唤醒那些存放元素的线程
        if (a == capacity) {
            signalNotFull();
        }

        return t;
    }

    @Override
    public void put(T t) {
        checkNull(t);

        ReentrantLock putLock = this.putLock;
        int a = -1;

        putLock.lock();

        // 队列中元素满了就一直等待
        try {
            while(count.get() == capacity) {
                notFull.await();
            }

            enqueue(t);
            // 有空间可以方了.数量加1
            a = count.getAndIncrement();

            // 如果当前线程放入一个元素后还有空间，则唤醒那些被阻塞的
            // 方元素线程
            if ((a + 1) < capacity) {
                notFull.signal();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            putLock.unlock();
        }

        // a == 0 代表在当前线程放入元素之前，队列为空，那么可能有阻塞的取元素线程
        if (a == 0) {
            signalNotEmpty();
        }
    }

    public int getCount() {
        return count.get();
    }

    private void signalNotEmpty() {
        ReentrantLock takeLock = this.takeLock;
        Condition notEmpty = this.notEmpty;
        takeLock.lock();
        try {
            notEmpty.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            takeLock.unlock();
        }
    }

    private void signalNotFull() {
        ReentrantLock putLock = this.putLock;
        Condition notFull = this.notFull;
        putLock.lock();

        try {
            notFull.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            putLock.unlock();
        }
    }

    // 入队和出队操作
    private void enqueue(T t) {
        last = last.next = new Node(t);
    }

    // 出队操作
    private T dequeue() {
        T t = null;
        // 主要逻辑是将头节点的后续节点元素取出，并将原先的后续节点设置为头节点
        Node first = this.first;
        Node next = first.next;

        t = (T) next.item;

        // 原先头节点和当前节点的指针和元素置为空
        first.next = null;
        next.item = null;
        //当前节点设置为头节点
        this.first = next;

        return t;
    }


    private void checkNull(T t) {
        if (null == t) {
            throw new NullPointerException("element can not be null");
        }
    }
}
