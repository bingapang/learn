package com.binga.jvm.memoryallocate;

/**
 * @Description: 对象优先在eden区分配
 *              -Xms20m -Xmx20m -Xmn10m -XX:+PrintGCDetails -XX:SurvivorRatio=8 -verbose:gc
 * @Author: binga
 * @Date: 2020/9/8 12:29
 * @Blog: https://blog.csdn.net/pang5356
 */
public class EdenAllocateFirstTest {

    public static final int _1M = 1024 * 1024;

    public static void main(String[] args) {
        byte[] a1, a2, a3;

        a1 = new byte[2 * _1M];
        a2 = new byte[2 * _1M];
        // 为a3分配内存之前会进行一次Minor GC ，默认占用了2MB多的空间，同时a1和a2进入
        // 老年代，Minor GC完后，将a3在eden区分配，可以看到eden占用了25%，也就是2MB
        a3 = new byte[2 * _1M];
    }
}
