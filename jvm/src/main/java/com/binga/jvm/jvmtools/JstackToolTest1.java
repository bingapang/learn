package com.binga.jvm.jvmtools;

/**
 * @Description: jstack 线程状态测试
 * @Author: binga
 * @Date: 2020/9/15 14:44
 * @Blog: https://blog.csdn.net/pang5356
 */
public class JstackToolTest1 {

    public static final Object LOCK = new Object();

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new Task(), "t1");
        Thread t2 = new Thread(new Task(), "t2");

        t1.start();
        t2.start();

        Thread.sleep(Integer.MAX_VALUE);
    }

    static class Task implements Runnable {

        @Override
        public void run() {
            synchronized (LOCK) {
                calculate();
            }
        }

        public void calculate() {
            int count = 0;
            while(true) {
                count++;
            }
        }
    }
}
