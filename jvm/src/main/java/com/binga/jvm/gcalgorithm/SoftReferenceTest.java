package com.binga.jvm.gcalgorithm;

import java.lang.ref.SoftReference;

/**
 * @Description: 引用类型
 *               -Xms20m -Xmx20m -Xmn10m -verbose:gc -XX:+PrintGCDetails -XX:PretenureSizeThreshold=20971520 -XX:+UseConcMarkSweepGC
 * @Author: binga
 * @Date: 2020/9/9 21:00
 * @Blog: https://blog.csdn.net/pang5356
 */
public class SoftReferenceTest {

    public static final int _1MB = 1024 * 1024;

    public static void main(String[] args) {
        // 软引用
        SoftReference<User> user1 = new SoftReference<>(new User());
        SoftReference<User> user2 = new SoftReference<>(new User());
        SoftReference<User> user3 = new SoftReference<>(new User());
        SoftReference<User> user4 = new SoftReference<>(new User());
    }

    static class User {
        private byte[] m = new byte[4 * _1MB];
    }
}

