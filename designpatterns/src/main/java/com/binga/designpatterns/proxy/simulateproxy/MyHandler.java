package com.binga.designpatterns.proxy.simulateproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @Description: handler
 * @Author: binga
 * @Date: 2020/8/25 15:30
 * @Blog: https://blog.csdn.net/pang5356
 */
public class MyHandler implements InvocationHandler {

    private Object object;

    public MyHandler(Object object) {
        this.object = object;
    }


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object obj = null;
        System.out.println("method before");
        if (proxy != null)
            obj =  method.invoke(this.object, args);

        System.out.println("method after");
        return obj;
    }
}
