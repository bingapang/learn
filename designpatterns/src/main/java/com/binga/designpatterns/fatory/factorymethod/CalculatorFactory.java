package com.binga.designpatterns.fatory.factorymethod;

/**
 * @Description: 薪酬计算器工厂抽象
 * @Author: binga
 * @Date: 2020/8/20 10:16
 * @Blog: https://blog.csdn.net/pang5356
 */
public interface CalculatorFactory {

    /**
     * 功能描述: 用于生产薪酬计算器的方法
     * @param: []
     * @return: com.binga.designpatterns.fatory.factorymethod.SalaryCalculator
     * @auther: binga
     * @date: 2020/8/20 10:16
     */
    SalaryCalculator calculator();
}
