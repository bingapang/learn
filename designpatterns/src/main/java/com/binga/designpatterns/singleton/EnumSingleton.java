package com.binga.designpatterns.singleton;

/**
 * @Description: 使用枚举实现的单例模式
 * @Author: binga
 * @Date: 2020/8/20 17:28
 * @Blog: https://blog.csdn.net/pang5356
 */
public enum EnumSingleton {
    INSTANCE;
}
