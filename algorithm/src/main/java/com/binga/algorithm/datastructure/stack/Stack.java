package com.binga.algorithm.datastructure.stack;

/**
 * @Description: 栈的抽象数据类型（ADT）
 *               栈是一种只能从表尾添加和删除元素的线性表结构，这里所谓的添加和
 *               删除也就是常说的入栈（push）和出栈(pop)。
 *               栈是一种后进先出(Last In First Out, LIFO)的数据结构。
 * @Author: binga
 * @Date: 2020/12/7 15:09
 * @Blog: https://blog.csdn.net/pang5356
 */
public interface Stack {

    /**
     * 功能描述: 栈中元素个数
     * @param: []
     * @return: int
     * @auther: binga
     * @date: 2020/12/7 15:12
     */
    int size();

    /**
     * 功能描述: 压栈，将数据元素添加至栈中
     * @param: [elem]
     * @return: void
     * @auther: binga
     * @date: 2020/12/7 15:13
     */
    void push(Object elem);

    /**
     * 功能描述: 出栈，将数据元素弹出栈，从栈中删除该元素
     * @param: []
     * @return: java.lang.Object
     * @auther: binga
     * @date: 2020/12/7 15:14
     */
    Object pop();

    /**
     * 功能描述: 获取栈顶的元素，但是不从栈中删除该元素
     * @param: []
     * @return: java.lang.Object
     * @auther: binga
     * @date: 2020/12/7 15:15
     */
    Object peek();
}
