package com.binga.designpatterns.fatory.abstractfactory;

/**
 * @Description: NikeTShirt
 * @Author: binga
 * @Date: 2020/8/20 11:00
 * @Blog: https://blog.csdn.net/pang5356
 */
public class NikeTShirt implements TShirt {

    public void show() {
        System.out.println("nike T-shirt");
    }
}
