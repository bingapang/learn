package com.binga.java.algorithm;

import java.util.Random;

/**
 * @Description: 排序算法实例
 * @Author: binga
 * @Date: 2020/11/11 14:26
 * @Blog: https://blog.csdn.net/pang5356
 */
public class SortAlgorithmDemo {

    public static int[] createArray() {
        int [] array = new int[10];

        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            array[i] = random.nextInt(100);
        }
        printArray(array);
        return array;
    }

    public static void printArray(int[] array) {
        if (null == array) return;

        System.out.print("[");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]);

            if (i != array.length -1) {
                System.out.print(",");
            }
        }
        System.out.println("]");
    }

    public static void main(String[] args) {
        int[] array = createArray();
        // 冒泡排序
//        bubbleSort1(array);
//        printArray(array);

        // 选择排序
//        selectSort1(array);
//        printArray(array);

        // 插入排序
        insertSort1(array);
        printArray(array);


    }

    /**
     * 冒泡排序 稳定
     */
    public static void bubbleSort(int[] array) {

        if (null == array) return;

        int temp;
        int len = array.length;
        for (int i = 0; i < len - 1; i++) {
            for (int j = 0; j < len - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }

    /**
     * 选择排序  [R0,..Ri]  [Ri+1,Rn]  不稳定
     */
    public static void selectSort(int[] array) {
        if (null == array) return;

        int minIndex;
        int temp;
        int len = array.length;

        for (int i = 0; i < len - 1; i++) {
            minIndex = i;
            for (int j = i + 1; j < len ; j++) {
                if (array[j] < array[minIndex]) {
                    minIndex = j;
                }
            }

            temp = array[i];
            array[i] = array[minIndex];
            array[minIndex] = temp;
        }
    }

    /**
     * 插入排序   稳定
     */
    public static void insertSort(int[] array) {
        if (null == array) return;

        int temp;
        int preIndex;
        int len = array.length;

        // 从1开始就是先把第一个元素作为已经排好序的
        for (int i = 1; i < len; i++) {
            temp = array[i];
            // 从已经排好序的最后一个元素开始比较
            preIndex = i - 1;
            while (preIndex >= 0 && array[preIndex] > temp) {
                // 元素向后移动一位，之前的位置有元素但是丢弃
                array[preIndex + 1] = array[preIndex];
                // 往前挪
                preIndex--;
            }

            // 找到位置，吧元素放入
            array[preIndex + 1] = temp;
        }
    }

    public static void bubbleSort1(int[] array) {
        if (null == array) return;

        int len = array.length;
        int temp;

        for (int i = 0; i < len - 1; i++) {
            for (int j = 0; j < len - 1 - i; j++) {
                if (array[j] > array[j + 1]) {
                    temp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = temp;
                }
            }
        }
    }

    public static void selectSort1(int[] array) {
        if (null == array) return;

        int len = array.length;
        int temp;
        int minIndex;
        for (int i = 0; i < len - 1 ; i++) {
            minIndex = i;

            for (int j = i + 1; j < len; j++) {
                if (array[j] < array[minIndex]) {
                    minIndex = j;
                }
            }

            temp = array[i];
            array[i] = array[minIndex];
            array[minIndex] = temp;
        }
    }

    public static void insertSort1(int[] array) {
        if (null == array) return;

        int len = array.length;
        int current;
        int preIndex;

        for (int i = 1; i < len ; i++) {
            preIndex = i - 1;
            current = array[i];

            while(preIndex >= 0 && array[preIndex] > current) {
                array[preIndex + 1] = array[preIndex];
                preIndex--;
            }
            array[preIndex+1] = current;
        }
    }
}
