package com.binga.designpatterns.singleton;

/**
 * @Description: 饿汉式单例模式
 * @Author: binga
 * @Date: 2020/8/20 17:06
 * @Blog: https://blog.csdn.net/pang5356
 */
public class HungrySingleton {

    private static HungrySingleton instance = new HungrySingleton();

    private HungrySingleton() {
        if (null != instance) {
            throw new RuntimeException("单例模式不允许创建多个实例");
        }

    }
    public static HungrySingleton getInstance() {
        return instance;
    }

}
