package com.binga.designpatterns.fatory.abstractfactory;

/**
 * @Description: AdidasShoes
 * @Author: binga
 * @Date: 2020/8/20 11:06
 * @Blog: https://blog.csdn.net/pang5356
 */
public class AdidasShoes implements Shoes {
    public void disPlay() {
        System.out.println("AdidasShoes");
    }
}
