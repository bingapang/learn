package com.binga.algorithm.datastructure.lineartable;

/**
 * @Description: 线性表ADT（抽象数据类型）
 *               线性表在这里约定index的值，为 0 <= index <= size -1,
 *               不再范围内则为索引越界
 * @Author: binga
 * @Date: 2020/12/3 11:09
 * @Blog: https://blog.csdn.net/pang5356
 */
public interface List {

    /**
     * 功能描述: 返回线性表中元素的个数
     * @param: []
     * @return: int
     * @auther: binga
     * @date: 2020/12/3 11:11
     */
    int length();

    /**
     * 功能描述: 根据下标获取线性表中指定的元素
     * @param: [index]
     * @return: java.lang.Object
     * @auther: binga
     * @date: 2020/12/3 11:13
     */
    Object get(int index);

    /**
     * 功能描述: 向指定的下标存储元素
     * @param: [index, elem]
     * @return: void
     * @auther: binga
     * @date: 2020/12/3 11:14
     */
    void insert(int index, Object elem);

    /**
     * 功能描述: 删除指定下标的元素
     * @param: [index]
     * @return: void
     * @auther: binga
     * @date: 2020/12/3 11:15
     */
    Object delete(int index);
}
