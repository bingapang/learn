package com.binga.jvm.classloader.classloading;

/**
 * @Description: 类加载时机测试
 * @Author: binga
 * @Date: 2020/8/18 21:39
 * @Blog: https://blog.csdn.net/pang5356
 */
public class ClassLoadOpportunityTest {

    public static void main(String[] args) {
        //System.out.println(SuperClass.age);
        //SubClass.print();

        //SuperClass[] array = new SuperClass[10];
        System.out.println(ConstantClass.CONSTANT);
    }
}


class SuperClass {

    static {
        System.out.println("super class init");
    }
    public static int age = 8;

    public static void print() {
        System.out.println("hello");
    }

}

class SubClass extends SuperClass {
     static {
         System.out.println("sub class init");
     }

    public static void print() {
        System.out.println("hi");
    }
}


class ConstantClass {
    public static final String CONSTANT = "CONSTANT";

    static {
        System.out.println("ConstantClass init");
    }
}