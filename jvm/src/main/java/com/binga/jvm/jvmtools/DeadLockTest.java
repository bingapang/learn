package com.binga.jvm.jvmtools;

/**
 * @Description: 死锁测试
 * @Author: binga
 * @Date: 2020/9/15 14:55
 * @Blog: https://blog.csdn.net/pang5356
 */
public class DeadLockTest {

    public static final Object LOCK1 = new Object();
    public static final Object LOCK2 = new Object();

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new Task1(), "t1");
        Thread t2 = new Thread(new Task2(), "t2");

        t1.start();
        t2.start();

        Thread.sleep(Integer.MAX_VALUE);
    }

    static class Task1 implements Runnable {

        @Override
        public void run() {
            synchronized (LOCK1) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                synchronized (LOCK2) {
                    System.out.println("get it");
                }
            }
        }
    }

    static class Task2 implements Runnable {

        @Override
        public void run() {
            synchronized (LOCK2) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                synchronized (LOCK1) {
                    System.out.println("get it");
                }
            }
        }
    }
}
