package com.binga.java.datastructure;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @Description:
 * @Author: binga
 * @Date: 2020/10/18 15:28
 * @Blog: https://blog.csdn.net/pang5356
 */
public class MathDemo {

    public static void main(String[] args) throws ParseException, InterruptedException {
        SimpleDateFormat f1 = new SimpleDateFormat("yyyy-MM-dd");
        Date d1 = f1.parse("2017-10-01");
        Date d2 = f1.parse("2019-08-16");


        long t = d2.getTime() - d1.getTime();
        long days = t /  (1000 * 60 * 60 * 24);
        System.out.println(new BigDecimal(days).divide(new BigDecimal(365), 0, BigDecimal.ROUND_HALF_UP));
    }
}
