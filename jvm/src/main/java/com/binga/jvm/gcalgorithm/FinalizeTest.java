package com.binga.jvm.gcalgorithm;

/**
 * @Description: finalize方法测试
 * @Author: binga
 * @Date: 2020/9/10 11:21
 * @Blog: https://blog.csdn.net/pang5356
 */
public class FinalizeTest {

    public static  FinalizeTest SAVE_HOOK = null;

    public static void main(String[] args) throws InterruptedException {
        SAVE_HOOK = new FinalizeTest();
        SAVE_HOOK = null;
        // 触发GC
        System.gc();
        // 因为finalize被一个地优先级的线程执行，所以等待500ms
        Thread.sleep(500);

        if (SAVE_HOOK != null) {
            SAVE_HOOK.isAlive();
        } else {
            System.out.println(" i am died");
        }

        SAVE_HOOK = null;
        System.gc();
        Thread.sleep(500);

        if (SAVE_HOOK != null) {
            SAVE_HOOK.isAlive();
        } else {
            System.out.println(" i am died");
        }
    }

    public void isAlive() {
        System.out.println("i am still alive");
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("finalize method executed");
        // 自救
        SAVE_HOOK = this;
    }
}
