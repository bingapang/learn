package com.binga.designpatterns.fatory.abstractfactory;

/**
 * @Description: nike工厂
 * @Author: binga
 * @Date: 2020/8/20 11:08
 * @Blog: https://blog.csdn.net/pang5356
 */
public class NikeClothingFactory implements ClothingFactory {

    public TShirt productTShirt() {
        return new NikeTShirt();
    }

    public Shoes productShoes() {
        return new NikeShoes();
    }
}
