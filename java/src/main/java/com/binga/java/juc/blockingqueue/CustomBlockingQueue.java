package com.binga.java.juc.blockingqueue;

/**
 * @Description: 自定义阻塞队列接口
 * @Author: binga
 * @Date: 2020/11/5 14:39
 * @Blog: https://blog.csdn.net/pang5356
 */
public interface CustomBlockingQueue<T> {

    T take();
    void put(T t);
}
