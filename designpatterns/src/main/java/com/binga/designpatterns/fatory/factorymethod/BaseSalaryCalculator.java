package com.binga.designpatterns.fatory.factorymethod;

/**
 * @Description: 基本工资计算器
 * @Author: binga
 * @Date: 2020/8/20 10:05
 * @Blog: https://blog.csdn.net/pang5356
 */
public class BaseSalaryCalculator implements SalaryCalculator {

    // 基本工资是通过员工的角色和职级定薪配置的，所以通过指标名称查询指定人员的基本工资
    public static final String BASE_SALARY_INDEX = "baseSalary";

    public void calculate(String userCode) {
        // 1.通过人员代码查询人员的角色直接
        System.out.println("query " + userCode + " role and dutylevel");
        // 2.通过角色直接及薪酬项查询人员的基本工资
        System.out.println("calculate " + userCode + " BaseSalary");
        // 3.保存人员基本工资，便于后续发薪使用
        System.out.println("save " + userCode + " baseSalary");
    }
}
