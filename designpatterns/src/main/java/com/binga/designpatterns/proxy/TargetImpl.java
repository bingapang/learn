package com.binga.designpatterns.proxy;

/**
 * @Description: 接口实现
 * @Author: binga
 * @Date: 2020/8/25 10:28
 * @Blog: https://blog.csdn.net/pang5356
 */
public class TargetImpl implements TargetInterface {
    public void doSomething() {
        System.out.println("TargetImpl method");
    }
}
