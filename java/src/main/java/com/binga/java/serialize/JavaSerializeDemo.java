package com.binga.java.serialize;


import java.io.*;

/**
 * @Description: java序列化示例
 * @Author: binga
 * @Date: 2020/11/17 09:40
 * @Blog: https://blog.csdn.net/pang5356
 */
public class JavaSerializeDemo {


    public static void main(String[] args) throws IOException {
        //write();


        read();

    }

    public static void write() throws IOException {
        User user = new User();
        user.setAddress("beijing");
        user.setFlag(1);
        user.setName("binga");
        user.setAge(18);
//        City beijing = City.BEIJING;
//        System.out.println(beijing.getName());

        ObjectOutputStream out = null;
        try {
            File file = new File("user.txt");
            System.out.println(file.getAbsolutePath());
            out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
            out.writeObject(user);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.flush();
            out.close();
        }
    }


    public static void read() throws IOException {
        File file = new File("user.txt");

        ObjectInputStream in = null;

        try {
            in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)));
            User user = (User) in.readObject();
            System.out.println(user);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            in.close();
        }
    }
}


enum City{
    BEIJING("BeiJing", 200), TIANJIN("TianJin", 100);

    private String name;
    private int age;

    City(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

}

class Person  {

    //private static final long serialVersionUID = -1L;

    private  int flag;
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }
}

class User extends Person implements Serializable {

    private static final long serialVersionUID = -1L;

    private String name;
    private int age;
    private int count;
    private String address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", count=" + count +
                ", address='" + address + '\'' +
                '}';
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}

