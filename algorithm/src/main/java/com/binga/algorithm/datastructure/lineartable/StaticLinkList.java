package com.binga.algorithm.datastructure.lineartable;

/**
 * @Description: 静态链表
 *               静态链表是处理不支持指针的编程语言来实现链表的数据接口
 *               静态链表实现思路如下：
 *               静态链表仍然使用的是数组，当时数组中每一个元素存储的不再单单的
 *               数据，还有当前元素的下一个元素的下标。同时数组的第一个元素做特殊的
 *               处理，其元素的下标数据用于存储当前链表的下一个使用存放元素下标。
 *               数组的最后一个元素也做特殊处理，用于存储链表中第一个元素的下标。
 *
 * @Author: binga
 * @Date: 2020/12/4 16:26
 * @Blog: https://blog.csdn.net/pang5356
 */
public class StaticLinkList implements List {

    private static final int DEFAULT_TABLE_SIZE = 1000;

    // 存储元素的数组
    private Node[] table;
    // 记录当前链表中元素个数
    private int size;

    // 构造方法
    public StaticLinkList() {
       this.table = new Node[DEFAULT_TABLE_SIZE];
       this.size = 0;
       init();
    }

    /**
     * 功能描述: 初始化静态链表
     *          1.初始化数组的第一个元素，用于记录链表中可用空间的第一个元素下标
     *          2.初始化最后一个元素，用于记录链表中第一个元素的下标
     *          3.初始化链表中真个备用链的连接关系
     * @param: []
     * @return: void
     * @auther: binga
     * @date: 2020/12/4 16:38
     */
    private void init() {
        // 初始化头节点
        Node head = new Node();
        head.next = 1;
        table[0] = head;

        // 初始化尾节点
        Node tail = new Node();
        // 当链表中元素个数为0时，则尾部元素记录的下标为0，代表没有下一个元素
        tail.next = 0;
        table[DEFAULT_TABLE_SIZE - 1] = tail;

        for (int i = 1; i < DEFAULT_TABLE_SIZE - 2; i++) {
            Node node = new Node();
            node.next = i + 1;
            table[i] = node;
        }

        // 倒数第二个没有后继节点则其next为0
        Node node = new Node();
        table[DEFAULT_TABLE_SIZE - 2] = node;
    }

    public int length() {
        return this.size;
    }

    /**
     * 功能描述: 从静态链表中获取指定index的元素
     *          1、首先判断index是否合法
     *          2、从数据最后一个元素开始遍历找到指定index的元素
     * @param: [index]
     * @return: java.lang.Object
     * @auther: binga
     * @date: 2020/12/4 16:47
     */
    public Object get(int index) {
        if (0 == size) throw new RuntimeException("list is empty");
        if (index < 0 || index >= size) throw new IndexOutOfBoundsException("index is not illegal");

        // 先从第一个元素获取,一直获取到需要元素的前一个元素
        int k = table.length - 1;
        for (int i = 0; i <= index; i++) {
            k = table[k].next;
        }

        // 获取元素
        return table[k].data;
    }
    
    /**
     * 功能描述: 将数据添加index指定的位置
     *          1、首先校验线性表是否满了
     *          2、遍历找到index的前一个元素
     *          3、找到元素后修改链表指针关系
     * @param: [index, elem]
     * @return: void
     * @auther: binga
     * @date: 2020/12/4 17:42
     */
    public void insert(int index, Object elem) {
        if (index < 0 || index > size) throw new IndexOutOfBoundsException("index is not illegal");

        // 获取到新元素添加的位置并将数据添加至队列中
        int insert = alloc();
        Node node = new Node();
        node.data = elem;
        table[insert] = node;

        // 接下来获取插入的index的前一个元素的下标
        int k = table.length - 1;
        // 先找到要插入位置的前一个元素位置
        for (int i = 0; i < index; i++) {
            k = table[k].next;
        }

        // 变更引用关系
        table[insert].next = table[k].next;
        table[k].next = insert;

        size++;
    }

    /**
     * 功能描述: 根据指定的index从线性表中剔除元素
     *          1、判断index是否越界
     *          2、将释放的元素的空间释放
     * @param: [index]
     * @return: java.lang.Object
     * @auther: binga
     * @date: 2020/12/4 17:57
     */
    public Object delete(int index) {
        if (index < 0 || index >= size) throw new IndexOutOfBoundsException("index is not illegal");

        // 先找到删除元素index的前一个元素
        int k = table.length - 1;
        for (int i = 0; i < index; i++) {
            k = table[k].next;
        }

        // 获取到index的下标
        int i = table[k].next;
        // 将index对应的元素存储
        Object old = table[i].data;

        // 变换引用关系
        table[k].next = table[i].next;
        // 释放位置
        free(i);
        size--;
        return old;
    }

    /**
     * 功能描述: 获取新插入元素的下标位置
     * @param: []
     * @return: int
     * @auther: binga
     * @date: 2020/12/4 17:28
     */
    private int alloc() {
        if (size == (table.length - 2)) throw new RuntimeException("list is full");
        Node head = table[0];
        // 应为头部的next存储的是插入元素的位置，所以获取到下一个元素的位置
        int next = head.next;

        // 将next的下一个位置赋值给head的next用于下次位置获取
        head .next = table[next].next;
        return next;
    }

    /**
     * 功能描述: 数据空间释放
     *          index是空闲出来的元素，那么index这个位置就需要添加至
     *          备用链中
     * @param: [index]
     * @return: void
     * @auther: binga
     * @date: 2020/12/4 17:36
     */
    private void free(int index) {
        table[index].data = null;
        table[index].next = table[0].next;
        table[0].next = index;
    }

    static class Node {
        Object data;
        // 没有下一个元素，则next为0
        int next;
    }
}
