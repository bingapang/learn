package com.binga.jvm.escapeanalysis;

/**
 * @Description: 逃逸分析值标量替换
 *        VM args: -Xms64m -Xmx64m -XX:+PrintGC -XX:+DoEscapeAnalysis -XX:+EliminateAllocations
 * @Author: binga
 * @Date: 2020/8/28 16:53
 * @Blog: https://blog.csdn.net/pang5356
 */
public class ScalarSubstitutionTest {

    public static void main(String[] args) {
        Test test = new Test();
        while (true) {
            test.test(10);
        }
    }
}

class Test {

    public int test(int value) {
        int value1 = value + 2;
        Point point = new Point(value1, 30);
        return point.getX();
    }
}

class Point {
    private int x;
    private int y;

    Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
