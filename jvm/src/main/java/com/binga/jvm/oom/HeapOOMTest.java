package com.binga.jvm.oom;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 堆内存溢出测试
 *                  -Xmx64m -Xms64m -XX:+PrintGC -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=C:\Users\Administrator\Desktop\
 * @Author: binga
 * @Date: 2020/8/26 14:24
 * @Blog: https://blog.csdn.net/pang5356
 */
public class HeapOOMTest {

    public static void main(String[] args) {
        List<User> users = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            Thread t1 = new Thread(new Runnable() {
                @Override
                public void run() {
                    for (;;) {
                        users.add(new User());
                    }
                }
            }, "thread" + i);
            t1.start();
        }
    }
}

class User {
    private byte[] _data = new byte[1024]; // 1kb
}
