package com.binga.jvm.classloader.brealparentaldelega;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;

/**
 * @Description: 打破双亲委派机制的类加载器
 * @Author: binga
 * @Date: 2020/8/18 17:37
 * @Blog: https://blog.csdn.net/pang5356
 */
public class BreakParentalDelegaClassLoader extends ClassLoader {

    private final String sourcePath;

    public BreakParentalDelegaClassLoader(String sourcePath) {
        this.sourcePath = sourcePath;
    }

    /**
     * 功能描述: 通过重载loadClass方法，变更原先的双签委派逻辑，
     * 而是有当前类加载器直接去配置的路径中查找字节码文件并加载
     * @param: [name, resolve]
     * @return: java.lang.Class<?>
     * @auther: binga
     * @date: 2020/8/18 17:47
     */
    @Override
    protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
        synchronized (getClassLoadingLock(name)) {
            // 不是自己指定的类则使用双亲委派
            if (!"com.binga.jvm.classloader.User".equals(name))
                return super.loadClass(name, false);

            // 首先检查类是否加载过
            Class<?> c = findLoadedClass(name);

            /**
             * 接下来不再像ClassLoader中的loadClass的逻辑一样，通过
             * 父类加载器加载，而是通过自己的findClass进行类的查找和加载
             */
            if (c == null) {
                long t0 = System.nanoTime();
                long t1 = System.nanoTime();
                c = findClass(name);

                // this is the defining class loader; record the stats
                sun.misc.PerfCounter.getParentDelegationTime().addTime(t1 - t0);
                sun.misc.PerfCounter.getFindClassTime().addElapsedTimeFrom(t1);
                sun.misc.PerfCounter.getFindClasses().increment();
            }

            if (c == null) {
                throw new ClassNotFoundException();
            }

            if (resolve) {
                resolveClass(c);
            }
            return c;
        }
    }

    /**
     * 功能描述: 重载CLassLoader的findClass方法，在该方法中通过指定的path，通过
     *          指定的类的全限定名称转换为路径从而读取class文件，然后通defineClass方法
     *          加载类并返回指定类的Class对象。
     * @param: [name]
     * @return: java.lang.Class<?>
     * @auther: binga
     * @date: 2020/8/18 17:03
     */
    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        byte[] byteCode = getByteArrayFromClassName(name);
        return defineClass(name, byteCode, 0, byteCode.length);
    }

    /**
     * 功能描述: 加载class文件返回字节数组
     * @param: [name]
     * @return: byte[]
     * @auther: binga
     * @date: 2020/8/18 17:05
     */
    private byte[] getByteArrayFromClassName(String name) throws ClassNotFoundException {
        String classPath = convertNameToPath(name);
        byte[] data = null;

        int off = 0;
        int length;

        try(BufferedInputStream bufferedInputStream =
                    new BufferedInputStream(new FileInputStream(classPath))) {
            data = new byte[bufferedInputStream.available()];
            while ((length = bufferedInputStream.read(data, off, data.length - off)) > 0) {
                off += length;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ClassNotFoundException();
        }
        return data;
    }

    /**
     * 功能描述: 类全限定名称转换为文件路径
     * @param: [name]
     * @return: java.lang.String
     * @auther: binga
     * @date: 2020/8/18 17:06
     */
    private String convertNameToPath(String name) {
        String classPath = name.replace(".", File.separator);
        classPath = sourcePath + File.separator + classPath + ".class";
        return classPath;
    }
}
