package com.binga.designpatterns.proxy;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * @Description: 代理模式测试
 * @Author: binga
 * @Date: 2020/8/25 10:15
 * @Blog: https://blog.csdn.net/pang5356
 */
public class ProxyTest {

    public static void main(String[] args) {

        TargetImpl target = new TargetImpl();
        TargetProxy proxy = new TargetProxy(target);
        TargetInterface proxyInstance = proxy.getProxyInstance();
        proxyInstance.doSomething();

//        TargetProxy proxy = new TargetProxy(null);
//        TargetInterface proxyInstance = proxy.getProxyInstance();
//        proxyInstance.toString();
    }
}
