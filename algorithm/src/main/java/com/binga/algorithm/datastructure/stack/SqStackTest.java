package com.binga.algorithm.datastructure.stack;

/**
 * @Description: 顺序存储结果实现栈结构测试
 * @Author: binga
 * @Date: 2020/12/7 15:30
 * @Blog: https://blog.csdn.net/pang5356
 */
public class SqStackTest {

    public static void main(String[] args) {
        Stack stack = new SqStack();

        // 添加10个元素
        for (int i = 0; i < 10; i++) {
            System.out.println(i);
            stack.push(i);
        }

        // 弹出5个
        for (int i = 0; i < 5; i++) {
            System.out.println(stack.pop());
        }

        // 弹出5个后栈顶元素
        System.out.println("当前栈顶元素" + stack.peek());

        // 再添加10个
        for (int i = 90; i < 100; i++) {
            stack.push(i);
        }

        // 弹出所有元素
        Integer temp = null;
        while (null != (temp = (Integer) stack.pop())) {
            System.out.println(temp);
        }

    }
}
