package com.binga.designpatterns.adapter;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.IOException;

/**
 * @Description: excel文件读取器
 * @Author: binga
 * @Date: 2020/8/20 18:10
 * @Blog: https://blog.csdn.net/pang5356
 */
public class ExcelFileReader implements FileReader {

    private Sheet sheet;
    // 行数位置 从所以考试
    private int readIndex = 0;

    public ExcelFileReader(String source) throws IOException {
        Workbook workbook = new HSSFWorkbook(ExcelFileReader.class.getResourceAsStream(source));
        sheet = workbook.getSheetAt(0);
    }

    public String[] readLine() {
        Row row = sheet.getRow(readIndex++);
        return new String[0];
    }

    public void close() throws IOException {
        if (null != sheet) {
            sheet.getWorkbook().close();
        }
    }
}
