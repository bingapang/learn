package com.binga.jvm.classloader.classloading;

/**
 * @Description: 接口加载时机测试
 * @Author: binga
 * @Date: 2020/8/18 22:27
 * @Blog: https://blog.csdn.net/pang5356
 */
public class InterfaceLoadOpportunityTest {

    public static void main(String[] args) {
        InterfaceImpl impl = new InterfaceImpl();

    }

}

interface SuperInterface {
    void doSuper();
}

interface SubInterface extends SuperInterface {
    void doSub();
}

class InterfaceImpl implements SubInterface {

    @Override
    public void doSuper() {
        System.out.println("super interface method");
    }

    @Override
    public void doSub() {
        System.out.println("sub interface method");
    }
}
