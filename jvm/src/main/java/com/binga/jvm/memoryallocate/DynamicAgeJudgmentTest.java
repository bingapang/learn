package com.binga.jvm.memoryallocate;

/**
 * @Description: 动态年龄判断
 *              -Xms60m -Xmx60m -Xmn20m -XX:+PrintGCDetails -XX:SurvivorRatio=8 -verbose:gc  -XX:MaxTenuringThreshold=15 -XX:+UseConcMarkSweepGC
 * @Author: binga
 * @Date: 2020/9/9 12:29
 * @Blog: https://blog.csdn.net/pang5356
 */
public class DynamicAgeJudgmentTest {

    public static final int _1MB = 1024 * 1024;

    public static void main(String[] args) {
        // 2.88M
        byte[] a1, a2, a3, a4;
        a1 = new byte[_1MB / 4];
        //a2 = new byte[_1MB / 4];
        a3 = new byte[8 * _1MB];
        a4 = new byte[8 * _1MB];
        a4 = null;
        a4 = new byte[8 * _1MB];
    }
}
