package com.binga.designpatterns.fatory.simplefactory;

/**
 * @Description: 简单工厂（静态工厂）测试代码
 * @Author: binga
 * @Date: 2020/8/20 09:32
 * @Blog: https://blog.csdn.net/pang5356
 */
public class SimpleFactoryTest {

    public static void main(String[] args) {
        // 获取苹果
        System.out.println(SimpleFactory.productFruit("apple"));
    }
}
