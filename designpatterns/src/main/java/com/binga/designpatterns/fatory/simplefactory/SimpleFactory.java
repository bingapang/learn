package com.binga.designpatterns.fatory.simplefactory;

/**
 * @Description: 简单工厂，创建水果实现
 * @Author: binga
 * @Date: 2020/8/20 09:41
 * @Blog: https://blog.csdn.net/pang5356
 */
public class SimpleFactory {

    public static Fruit productFruit(String fruitName) {
        if ("apple".equals(fruitName)) {
            return new Apple();
        } else if ("peach".equals(fruitName)) {
            return new Peach();
        }
        throw new RuntimeException("no such fruit:" + fruitName);
    }
}
