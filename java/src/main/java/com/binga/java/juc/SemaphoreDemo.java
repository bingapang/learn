package com.binga.java.juc;

import java.util.concurrent.Semaphore;

/**
 * @Description: SemaphoreDemo
 * @Author: binga
 * @Date: 2020/11/3 14:56
 * @Blog: https://blog.csdn.net/pang5356
 */
public class SemaphoreDemo {

    public static void main(String[] args) throws InterruptedException {
        Semaphore semaphore = new Semaphore(3);

        // 先开100个线程
        for (int i = 0; i < 100; i++) {
            new Thread(new Task(semaphore), "thread" + i).start();
        }

        Thread.sleep(Integer.MAX_VALUE);

    }

    static class Task implements Runnable {

        private final Semaphore semaphore;

        Task(Semaphore semaphore) {
            this.semaphore = semaphore;
        }

        @Override
        public void run() {

            try {
                semaphore.acquire(4);
                System.out.println(Thread.currentThread().getName() + "begin");
                Thread.sleep(1000);
                System.out.println(semaphore.availablePermits());
                System.out.println(Thread.currentThread().getName() + "end");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                semaphore.release();
            }
        }
    }
}
