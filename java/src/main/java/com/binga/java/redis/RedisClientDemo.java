package com.binga.java.redis;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFuture;
import org.redisson.RedissonBloomFilter;
import org.redisson.api.RFuture;
import org.redisson.client.RedisClient;
import org.redisson.client.RedisClientConfig;
import org.redisson.client.RedisConnection;
import org.redisson.client.handler.State;
import org.redisson.client.protocol.Decoder;
import org.redisson.client.protocol.RedisCommand;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.concurrent.ExecutionException;

/**
 * @Description: TODO
 * @Author: binga
 * @Date: 2020/11/13 11:25
 * @Blog: https://blog.csdn.net/pang5356
 */
public class RedisClientDemo {

    public static RedisClient redisClient;

    public static final String HOST = "localhost";
    public static final int PORT = 6379;

    static {
        RedisClientConfig redisClientConfig = new RedisClientConfig();
        redisClientConfig.setAddress(HOST, PORT);
        redisClientConfig.setDatabase(0);
        redisClientConfig.setPassword("xexd");
        redisClient = RedisClient.create(redisClientConfig);
    }

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        RedisConnection connect = redisClient.connect();

        // 设置值
        RedisCommand set = new RedisCommand("set");
        RFuture async = connect.async(set, "name", "binga");
        async.await();
        Object o = async.get();
        System.out.println(o);
        // 获取值
        RedisCommand get = new RedisCommand("get", new Decoder<String>() {
            @Override
            public String decode(ByteBuf byteBuf, State state) throws IOException {
                return byteBuf.toString(Charset.forName("utf-8"));
            }
        });
        RFuture name = connect.async(get, "name");
        name.await();
        System.out.println(name.get());

        // hash
        RedisCommand hset = new RedisCommand("hset");
        RFuture async1 = connect.async(hset, "user", "name", "binga");
        async1.await();
        System.out.println(async1.get());

        connect.closeAsync().sync();
    }


}
