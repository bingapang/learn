package com.binga.algorithm.datastructure.queue;

/**
 * @Description: 队列的抽象数据类型（ADT）:
 *               队列是一种只允许在一段进行数据的添加，而在另一端进行数据删除的线程表。
 *               队列允许删除的一端称之为队列头，而允许添加的一端称之为队列尾。队列是一种
 *               先进先出的数据结构（First In First Out, FIFO）。
 *
 * @Author: binga
 * @Date: 2020/12/9 14:32
 * @Blog: https://blog.csdn.net/pang5356
 */
public interface Queue {

    /**
     * 功能描述: 获取队列的长度（队列元素个数）
     * @param: []
     * @return: int
     * @auther: binga
     * @date: 2020/12/9 14:36
     */
    int length();

    /**
     * 功能描述: 判断队列是否为空
     * @param: []
     * @return: boolean
     * @auther: binga
     * @date: 2020/12/9 14:36
     */
    boolean isEmpty();

    /**
     * 功能描述: 参数元素添加至队列
     * @param: [elem]
     * @return: void
     * @auther: binga
     * @date: 2020/12/9 14:37
     */
    void enqueue(Object elem);

    /**
     * 功能描述: 从队列头部剔除元素
     * @param: []
     * @return: java.lang.Object
     * @auther: binga
     * @date: 2020/12/9 14:37
     */
    Object dequeue();
}
