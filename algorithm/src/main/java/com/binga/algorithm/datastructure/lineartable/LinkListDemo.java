package com.binga.algorithm.datastructure.lineartable;

/**
 * @Description: 链表实现的线性表测试代码
 * @Author: binga
 * @Date: 2020/12/3 18:12
 * @Blog: https://blog.csdn.net/pang5356
 */
public class LinkListDemo {

    public static void main(String[] args) {
        LinkList linkList = new LinkList();

        for (int i = 0; i < 20; i++) {
            System.out.println(i);
            linkList.insert(i, i);
        }

        for (int i = 0; i < 10; i++) {
            linkList.delete(i);
        }

        for (int i = 0; i < linkList.length(); i++) {
            System.out.println(linkList.get(i));
        }
    }
}
