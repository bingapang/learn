package com.binga.jvm.gcalgorithm;

/**
 * @Description: 引用计数法测试
 *                 -Xms20m -Xmx20m -Xmn10m -verbose:gc -XX:+PrintGCDetails
 * @Author: binga
 * @Date: 2020/9/9 20:31
 * @Blog: https://blog.csdn.net/pang5356
 */
public class ReferenceCountingTest {

    public static final int _1MB = 1024 * 1024;
    private byte[] m = new byte[2 * _1MB];

    ReferenceCountingTest obj;

    public static void main(String[] args) {
        ReferenceCountingTest objA = new ReferenceCountingTest();
        ReferenceCountingTest objB = new ReferenceCountingTest();

        objA.obj = objB;
        objB.obj = objA;

        objA = null;
        objB = null;

        System.gc();
    }
}
