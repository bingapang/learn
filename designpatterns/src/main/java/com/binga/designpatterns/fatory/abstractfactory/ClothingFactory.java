package com.binga.designpatterns.fatory.abstractfactory;

/**
 * @Description: 抽象工厂抽象，定义了生产产品族的方法
 * @Author: binga
 * @Date: 2020/8/20 11:07
 * @Blog: https://blog.csdn.net/pang5356
 */
public interface ClothingFactory {

    TShirt productTShirt();
    Shoes productShoes();
}
