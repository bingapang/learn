package com.binga.designpatterns.fatory.abstractfactory;

/**
 * @Description: NikeShoes
 * @Author: binga
 * @Date: 2020/8/20 11:05
 * @Blog: https://blog.csdn.net/pang5356
 */
public class NikeShoes implements Shoes {
    public void disPlay() {
        System.out.println("NikeShoes");
    }
}
