package com.binga.java.threadlocal;

/**
 * @Description: ThreadLocal test
 * @Author: binga
 * @Date: 2020/9/16 14:01
 * @Blog: https://blog.csdn.net/pang5356
 */
public class ThreadLocalTest {

    public static void main(String[] args) {
        Thread t1 = new Thread(new Task(), "t1");
        Thread t2 = new Thread(new Task(), "t2");
        Thread t3 = new Thread(new Task(), "t3");

        t1.start();
        t2.start();
        t3.start();
    }


    static class Task implements Runnable {

        public void run() {
            setValue();
            showValue();
            cleanValue();
        }

        public void setValue() {
            System.out.println(Thread.currentThread().getName() + " set value: " + Thread.currentThread().getName());
            ThreadContextHolder.set(Thread.currentThread().getName());
        }

        public void showValue() {
            System.out.println(Thread.currentThread().getName() + " value : " + ThreadContextHolder.get());
        }

        public void cleanValue() {
            ThreadContextHolder.clean();
        }
    }


    static class ThreadContextHolder {
        private static ThreadLocal<String> strHolder = new ThreadLocal<String>();

        public static void set(String str) {
            strHolder.set(str);
        }

        public static String get() {
            return strHolder.get();
        }

        public static void clean() {
            strHolder.remove();
        }
    }
}
