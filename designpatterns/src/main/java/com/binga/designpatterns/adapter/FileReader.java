package com.binga.designpatterns.adapter;

import java.io.Closeable;

/**
 * @Description: 声明文件读取接口，用于定义读取如excel,txt,unl等文件
 *                 这种文件的格式是以行为一个映射实体,因为是操作文件的则
 *                 会涉及的流，则这里为继承了Closeable接口
 * @Author: binga
 * @Date: 2020/8/20 18:03
 * @Blog: https://blog.csdn.net/pang5356
 */
public interface FileReader extends Closeable {

    /**
     * 功能描述: 定义读取行的方法，这里没有使用Object数组，模认数据均为字符串
     *          读取一行数据：
     *          对于excel的读取一行，将行数据填充值数组；
     *          对于txt数据，可能以""分割，可能以"@分割"，分割后填充至数组中
     *          当返回的数组长度为0时则证明没有数据了。
     * @param: []
     * @return: java.lang.String[][]
     * @auther: binga
     * @date: 2020/8/20 18:05
     */
    String[] readLine();
}
