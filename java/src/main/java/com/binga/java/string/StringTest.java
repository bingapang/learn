package com.binga.java.string;

/**
 * @Description: String 常量池测试
 * @Author: binga
 * @Date: 2020/9/22 18:12
 * @Blog: https://blog.csdn.net/pang5356
 */
public class StringTest {

    public static void main(String[] args) {
        String hello = "Hello", lo = "lo", hel = "Hel";

        String aa = hel + lo;
        String bb = "Hel" + "lo";
        String cc = hel + "lo";
        String dd = "Hel" + lo;
        String ee = new String("Hello");

        System.out.println(hello == aa);
        System.out.println(hello == bb);
        System.out.println(hello == cc);
        System.out.println(hello == dd);
        System.out.println(hello == ee);

        String intern = hello.intern();





    }
}
