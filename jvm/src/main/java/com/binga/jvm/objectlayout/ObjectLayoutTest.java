package com.binga.jvm.objectlayout;

import org.openjdk.jol.info.ClassLayout;

/**
 * @Description: 对象内存布局测试
 *                  -XX:-UseCompressedClassPointers -XX:-UseCompressedOops
 * @Author: binga
 * @Date: 2020/8/27 14:18
 * @Blog: https://blog.csdn.net/pang5356
 */
public class ObjectLayoutTest {

    public static void main(String[] args) {
        // Object对象
        ClassLayout objLayout = ClassLayout.parseInstance(new Object());
        System.out.println(objLayout.toPrintable());

        System.out.println("--------------------------------------------");

        // 数组对象
        ClassLayout arrayLayout = ClassLayout.parseInstance(new int[]{});
        System.out.println(arrayLayout.toPrintable());

        System.out.println("--------------------------------------------");

        // 自定义User对象
        ClassLayout userLayout = ClassLayout.parseInstance(new User());
        System.out.println(userLayout.toPrintable());
    }
}

class User {
    private String name; // 不开启指针压缩，引用类型占用8字节，开启后占4字节
    private int age;     // 4字节
    private byte sex;    // 1字节，并填充3字节
    private Object obj;  // 不开启指针压缩，引用类型占用8字节，开启后占4字节
}
