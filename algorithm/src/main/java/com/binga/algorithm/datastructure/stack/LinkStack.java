package com.binga.algorithm.datastructure.stack;

/**
 * @Description: 基于链表实现栈。
 *               基于链表实现的线性表，通过维护一个head指针，从而记录线性表的
 *               起始节点，那么通过链表实现栈结构可以通过维护一个top指针来记录
 *               栈的栈顶元素。当然链表实现的栈结构容量时没有限制的，因为其不像
 *               数组一样，还需要事先分配好数组空间。
 * @Author: binga
 * @Date: 2020/12/7 16:07
 * @Blog: https://blog.csdn.net/pang5356
 */
public class LinkStack implements Stack {

    // 记录栈中元素数量
    private int size;

    // 用于记录栈的栈顶元素，当栈元素为0是，其为null
    private StackNode top;

    public LinkStack() {

    }

    /**
     * 功能描述: 获取栈中元素数量
     * @param: []
     * @return: int
     * @auther: binga
     * @date: 2020/12/7 16:12
     */
    public int size() {
        return size;
    }

    /**
     * 功能描述: 将参数压入栈中
     * @param: [elem]
     * @return: void
     * @auther: binga
     * @date: 2020/12/7 16:12
     */
    public void push(Object elem) {
        StackNode node = new StackNode();
        node.data = elem;

        // 将原先的栈顶元素追加至新加入的元素后面
       node.next = top;
       // 新加入的元素为栈顶
       top = node;
       size++;
    }

    /**
     * 功能描述: 将栈中栈顶元素弹出并从栈中删除该元素
     * @param: []
     * @return: java.lang.Object
     * @auther: binga
     * @date: 2020/12/7 16:17
     */
    public Object pop() {
        // 栈空间中元素个数为0
        if (null == top) return null;

        StackNode oldTop = top;
        top = oldTop.next;
        oldTop.next = null;// help gc

        size--;
        return oldTop.data;
    }

    /**
     * 功能描述: 返回栈顶当前元素
     * @param: []
     * @return: java.lang.Object
     * @auther: binga
     * @date: 2020/12/7 16:24
     */
    public Object peek() {
        // 当栈中元素个数为0时，返回空
        if (null == top) return null;
        return top.data;
    }

    static class StackNode {
        Object data;
        StackNode next;
    }
}
