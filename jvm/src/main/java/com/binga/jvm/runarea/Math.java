package com.binga.jvm.runarea;

/**
 * @Description: 运行时区域之栈结构示例
 * @Author: binga
 * @Date: 2020/8/20 22:46
 * @Blog: https://blog.csdn.net/pang5356
 */
public class Math {

    public static void main(String[] args) {
        Math math = new Math();
        int compute = math.compute();
        System.out.println(compute);
    }

    public int compute() {
        int a = 1;
        int b = 2;
        int c = (a + b) * 10;
        return c;
    }
}
