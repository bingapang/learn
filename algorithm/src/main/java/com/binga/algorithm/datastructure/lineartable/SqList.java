package com.binga.algorithm.datastructure.lineartable;

/**
 * @Description:  顺序存储结构实现线性表
 *                所谓顺序存储结构就是向数组一样，同一类元素的集合，并且在内存中占用的空间时连续
 *                的。
 *                顺序存储结构实现的线性表优缺点如下：
 *                优点如下：
 *                  1>不需要使用额外的存储空间来表示数据元素之间的逻辑关系，他们的前后关系可以通过数据下标
 *                     进行计算
 *                  2>可以快速的获取线程表中任务位置的元素
 *                缺点如下：
 *                  1>对线性表中添加或者删除元素，均需要进行元素的移动
 *                  2>线性表容量变化较大时，难以确定存储的容量
 *                  3>造成存储空间的”碎片“
 * @Author: binga
 * @Date: 2020/12/3 11:15
 * @Blog: https://blog.csdn.net/pang5356
 */
public class SqList implements List {

    // 默认线性表有20个空间
    private static final int LIST_SIZE = 20;

    // 当前线性表中元素的个数，使用size记录
    private int size;

    // 用于存储元素的数组，也就是顺序存储结构
    private Object[] table;

    public SqList() {
        this.size = 0;
        this.table = new Object[LIST_SIZE];
    }

    public int length() {
        return this.size;
    }

    /**
     * 功能描述: 从线性表中根据指定的下标获取元素
     *          1、判断线性表是否为空
     *          2、判断序号参数是否越界
     *          3、返回指定序号的元素
     * @param: [index]
     * @return: java.lang.Object
     * @auther: binga
     * @date: 2020/12/3 11:29
     */
    public Object get(int index) {
        // 当前线性中不存在元素
        if (size == 0) {
            throw new RuntimeException("SqList is empty");
        }

        // 判断序号是否在合理的值内
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("index is not illegal");
        }
        return table[index];
    }

    /**
     * 功能描述: 根据指定的序号，将参数元素添加至线性表中，过程如下
     *          如果当前线程表已经满了，即 size == table.length,那么元素不可添加了
     *          同时，需要保证 0 <= index <= size,
     *          这里有几种插入情况
     *          1>元素添加至线性表的尾部，那么不需要移动元素
     *          2>将元素添加至头部，或者中间部位，则从index位置需要将每个元素向后移动一位
     *
     * @param: [index, elem]
     * @return: void
     * @auther: binga
     * @date: 2020/12/3 14:59
     */
    public void insert(int index, Object elem) {
        // 判断线性表是否已经满了
        if (size >= table.length) throw new RuntimeException("SqList is full");

        // 判断是都越界
        if (index < 0 || index > size) throw new IndexOutOfBoundsException("index is not illegal");

        // 插入线性表的尾部
        if (index == size) {
            table[size++] = elem;
            return;
        }

        /**
         *  不是尾部则需要从index开始，每个元素向后移动一位，
         *  这里是从最后一个位置开始移动，向前遍历，直到index
         */

        for (int i = size -1; i <= index ; i--) {
            table[i + 1] = table[i];
        }
        table[index] = elem;
        size++;
    }

    /**
     * 功能描述: 根据参数index，将元素从线性表中删除，
     *          删除有几种情况
     *          1>元素位于线性表的尾部，那么只需将元素置空并且将size-1即可
     *          2>元素位于中间或者头部，那么需要从index + 1 位置开始向后遍历
     *            每一个元素都向前移动一位
     * @param: [index]
     * @return: void
     * @auther: binga
     * @date: 2020/12/3 15:13
     */
    public Object delete(int index) {
        // 线性表为空
        if (size == 0) throw new RuntimeException("SqList is empty");

        // 确保index在合法范围内
        if (index < 0 || index >= size) throw new IndexOutOfBoundsException("index is not illegal");

        // 删除的就是尾部元素
        if (index == (size - 1 )) {
            Object elem = table[index];
            table[index] = null; //help gc
            size--;
            return elem;
        }

        Object elem = table[index];
        // 从index + 1 向后遍历。每个元素向前移动一位
        for (int i = (index + 1); i < size ; i++) {
            table[i -1] = table[i];
        }

        // 指定元素都向前移动一位，那么最后一个槽位需要腾出空间，置为空
        table[size - 1] = null; // help gc
        size--;
        return elem;
    }
}
