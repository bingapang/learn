package com.binga.designpatterns.singleton;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * @Description: 序列化测试单例实现能否保证单例
 * @Author: binga
 * @Date: 2020/8/20 17:31
 * @Blog: https://blog.csdn.net/pang5356
 */
public class SerializeTest {

    public static void main(String[] args) throws Exception {
        // 懒汉模式序列化测试
        LazySingleton lazySingleton = LazySingleton.getInstance();

        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("lazySingleton"));
        oos.writeObject(lazySingleton);
        oos.close();

        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("lazySingleton"));
        LazySingleton serialSingleton = (LazySingleton) ois.readObject();
        ois.close();

        System.out.println(lazySingleton == serialSingleton);

//        HungrySingleton hungrySingleton = HungrySingleton.getInstance();
//
//        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("hungrySingleton"));
//        oos.writeObject(hungrySingleton);
//        oos.close();
//
//        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("hungrySingleton"));
//        HungrySingleton serialSingleton = (HungrySingleton) ois.readObject();
//        ois.close();
//
//        System.out.println(hungrySingleton == serialSingleton);

//        StaticInnerClassSingleton staticInnerClassSingleton = StaticInnerClassSingleton.getInstance();
//
//        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("staticInnerClassSingleton"));
//        oos.writeObject(staticInnerClassSingleton);
//        oos.close();
//
//        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("staticInnerClassSingleton"));
//        StaticInnerClassSingleton serialSingleton = (StaticInnerClassSingleton) ois.readObject();
//        ois.close();
//
//        System.out.println(staticInnerClassSingleton == serialSingleton);

//        EnumSingleton enumSingleton = EnumSingleton.INSTANCE;
//
//        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("enumSingleton"));
//        oos.writeObject(enumSingleton);
//        oos.close();
//
//        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("enumSingleton"));
//        EnumSingleton serialSingleton = (EnumSingleton) ois.readObject();
//        ois.close();
//
//        System.out.println(enumSingleton == serialSingleton);
    }
}
