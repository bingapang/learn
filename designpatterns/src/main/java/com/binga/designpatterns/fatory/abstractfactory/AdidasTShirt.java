package com.binga.designpatterns.fatory.abstractfactory;

/**
 * @Description: AdidasTShirt
 * @Author: binga
 * @Date: 2020/8/20 10:54
 * @Blog: https://blog.csdn.net/pang5356
 */
public class AdidasTShirt implements TShirt {
    public void show() {
        System.out.println("adidas T-shirt");
    }
}
