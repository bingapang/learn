package com.binga.jvm.classloader;

/**
 * @Description: 定义被加载的类
 * @Author: binga
 * @Date: 2020/8/18 16:56
 * @Blog: https://blog.csdn.net/pang5356
 */
public class User {

    public void say(){
        System.out.println("hello");
    }

    public void print(String message) {
        System.out.println(message);
    }
}
