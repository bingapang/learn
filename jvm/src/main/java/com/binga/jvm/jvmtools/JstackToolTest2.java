package com.binga.jvm.jvmtools;

/**
 * @Description: jstack线程状态测试
 * @Author: binga
 * @Date: 2020/9/15 16:51
 * @Blog: https://blog.csdn.net/pang5356
 */
public class JstackToolTest2 {

    public static final Object LOCK = new Object();

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new Task(), "t1");
        Thread t2 = new Thread(new Task(), "t2");

        t1.start();
        t2.start();

        Thread.sleep(Integer.MAX_VALUE);

    }

    static class Task implements Runnable {

        @Override
        public void run() {
            synchronized (LOCK) {
                try {
                    LOCK.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
