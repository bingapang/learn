package com.binga.jvm.oom;

/**
 * @Description: 栈内存溢出  通过增加栈帧的大小
 * -Xmx64m -Xms64m -Xss128k -XX:+PrintGC
 * @Author: binga
 * @Date: 2020/8/26 22:46
 * @Blog: https://blog.csdn.net/pang5356
 */
public class StackOOMTest2 {

    public static int count = 0;

    public static void main(String[] args) {
        try {
            recurse();
        } catch (Throwable e) {
            System.out.println(count);
            e.printStackTrace();

        }
    }

    public static void recurse() {
        long unused1, unused2, unused3, unused4, unused5, unused6, unused7, unused8, unused9, unused10,
                unused11, unused12, unused13, unused14, unused15, unused16, unused17, unused18, unused19, unused20,
                unused21, unused22, unused23, unused24, unused25, unused26, unused27, unused28, unused29, unused30,
                unused31, unused32, unused33, unused34, unused35, unused36, unused37, unused38, unused39, unused40,
                unused41, unused42, unused43, unused44, unused45, unused46, unused47, unused48, unused49, unused50;
        count++;
        recurse();

        unused1 =  unused2 =  unused3 =  unused4 =  unused5 =  unused6 =  unused7 =  unused8 =  unused9 =  unused10 =
        unused11 =  unused12 =  unused13 =  unused14 =  unused15 =  unused16 =  unused17 =  unused18 =  unused19 =  unused20 =
        unused21 =  unused22 =  unused23 =  unused24 =  unused25 =  unused26 =  unused27 =  unused28 =  unused29 =  unused30 =
        unused31 =  unused32 =  unused33 =  unused34 =  unused35 =  unused36 =  unused37 =  unused38 =  unused39 =  unused40 =
        unused41 =  unused42 =  unused43 =  unused44 =  unused45 =  unused46 =  unused47 =  unused48 =  unused49 =  unused50 = 0;
    }
}
