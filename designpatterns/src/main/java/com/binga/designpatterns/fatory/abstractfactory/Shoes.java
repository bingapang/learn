package com.binga.designpatterns.fatory.abstractfactory;

/**
 * @Description: Shoes抽象
 * @Author: binga
 * @Date: 2020/8/20 11:01
 * @Blog: https://blog.csdn.net/pang5356
 */
public interface Shoes {
    void disPlay();
}
