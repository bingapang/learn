package com.binga.java.datastructure;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * @Description: 树遍历示例
 * @Author: binga
 * @Date: 2020/11/11 11:01
 * @Blog: https://blog.csdn.net/pang5356
 */
public class TreeDemo {

    static class TreeNode<T> {
        T item;
        TreeNode leftChild;
        TreeNode rightChild;

        TreeNode(T item, TreeNode leftChild, TreeNode rightChild) {
            this.item = item;
            this.leftChild = leftChild;
            this.rightChild = rightChild;
        }

        TreeNode(T item) {
            this.item = item;
        }
    }

    /**
     *          A
     *     /         \
     *    B           C
     *  /   \        /
     * D     E      F
     *      /
     *     G
     */
    public static TreeNode createTree() {
        // 根
        TreeNode<String> root = new TreeNode<>("A");
        root.leftChild = new TreeNode("B");
        root.rightChild = new TreeNode("C");

        root.leftChild.leftChild = new TreeNode("D");
        root.leftChild.rightChild = new TreeNode("E");
        root.rightChild.leftChild = new TreeNode("F");

        root.leftChild.rightChild.leftChild = new TreeNode("G");
        return root;
    }

    public static void main(String[] args) {
        // 前序遍历
        // A B D E G C F
        preOrderTravel(createTree());

        System.out.println();

        // 中序遍历
        // D B G E A F C
        inOrderTravel(createTree());

        System.out.println();

        // 后续遍历
        // D G E B F C A
        postOrderTravel(createTree());

        System.out.println();

        // 层次遍历
        // A B C D E F G
        levelTravel(createTree());

        System.out.println();
        // 层次分层遍历
        levelTravelDiv(createTree());

    }

    // 二叉树前序遍历啊  根 左 右
    // A B D E G C F
    public static void preOrderTravel(TreeNode root) {
        if (null == root) return;

        // 当前节点作为根节点输出
        System.out.print(root.item);
        // 左
        preOrderTravel(root.leftChild);
        // 右
        preOrderTravel(root.rightChild);
    }

    // 二叉树中序遍历  左 根 右
    // D B G E A F C
    public static void inOrderTravel(TreeNode root) {
        if (null == root) return;
        // 左
        inOrderTravel(root.leftChild);
        // 根
        System.out.print(root.item);
        // 右
        inOrderTravel(root.rightChild);
    }

    // 二叉树后续遍历  左 右 根
    // D G E B F C A
    public static void postOrderTravel(TreeNode root) {
        if (null == root) return;
        // 左
        postOrderTravel(root.leftChild);
        // 右
        postOrderTravel(root.rightChild);
        // 根
        System.out.print(root.item);
    }

    // 层次遍历
    /**
     *  层次遍历就是将树从顶层然后一层一层的由左至右的遍历
     *  ABCDEFG
     */
    public static void levelTravel(TreeNode root){
        if (null == root) return;

        Queue<TreeNode> queue = new ArrayDeque<>();
        // 先将根节店方法队列
        queue.add(root);

        // 保持队列不为空
        while (!queue.isEmpty()) {
            TreeNode poll = queue.poll();
            System.out.print(poll.item);

            if (null != poll.leftChild) {
                queue.add(poll.leftChild);
            }

            if (null != poll.rightChild) {
                queue.add(poll.rightChild);
            }
        }
    }


    // 层次遍历分层
    /**
     *          A
     *     /         \
     *    B           C
     *  /   \        /
     * D     E      F
     *      /
     *     G
     *
     *     输出为
     *      A
     *      BC
     *      DEF
     *      G
     */
    public static void levelTravelDiv(TreeNode root) {
        // 还是基于分层遍历进行遍历，但是需要区分层次的界限
        Queue<TreeNode> queue = new ArrayDeque<>();

        // 层次的最后一个节点
        TreeNode levelLast = root;
        // 记录下一层的每一个元素
        TreeNode nextLast = null;

        queue.add(root);

        while (!queue.isEmpty()) {
            TreeNode poll = queue.poll();
            System.out.print(poll.item);

            // 左
            if (null != poll.leftChild) {
                queue.add(poll.leftChild);
                // 记录下一层的每个节点
                nextLast = poll.leftChild;
            }

            // 右
            if (null != poll.rightChild) {
                queue.add(poll.rightChild);
                // 更改界限
                nextLast = poll.rightChild;
            }

            // 当前节点为层次最后一个节点则换层
            if (poll == levelLast) {
                System.out.print("\r\n");

                // 换层则将当前的层次换为下一层次
                levelLast = nextLast;
            }
        }
    }
}
