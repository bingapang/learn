package com.binga.java.juc.blockingqueue;


import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * @Description: 阻塞队列示例
 * @Author: binga
 * @Date: 2020/11/5 09:25
 * @Blog: https://blog.csdn.net/pang5356
 */
public class BlockingDemo {

    public static void main(String[] args) throws InterruptedException {
        CustomLinkedBlockingQueue<User> blockingQueue =
                new CustomLinkedBlockingQueue<>(100);
        CountDownLatch countDownLatch = new CountDownLatch(50);
        CountDownLatch countDownLatch2 = new CountDownLatch(45);
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 50; i++) {
                    Thread t1 = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(2);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            blockingQueue.put(new User());
                            System.out.println("put");
                            countDownLatch.countDown();
                        }
                    });
                    t1.start();
                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 45; i++) {
                    Thread t1 = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(1);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            System.out.println("take" + blockingQueue.take());
                            countDownLatch2.countDown();
                        }
                    });
                    t1.start();
                }
            }
        });

        t1.start();
        t2.start();
        countDownLatch.await();
        countDownLatch2.await();
        System.out.println(blockingQueue.getCount());
    }

    static class User {

    }
}
