package com.binga.java.axis2;



import com.google.gson.Gson;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;

import javax.xml.namespace.QName;
import java.rmi.RemoteException;

/**
 * @Description: TODO
 * @Author: binga
 * @Date: 2020/11/12 17:52
 * @Blog: https://blog.csdn.net/pang5356
 */
public class Axis2Demo {

    public static void main(String[] args) throws RemoteException {
        String reqXml = bean2Json(create());
        System.out.println(reqXml);
        String url = "http://10.10.206.99:8001/pdfbpoc/services/getUniCredit?wsdl";

        RPCServiceClient serviceClient = new RPCServiceClient();

        Options options = serviceClient.getOptions();
        //指定调用的WebService的URL
        EndpointReference er = new EndpointReference(url);
        options.setTo(er);

        //指定getInfo方法的参数值
        Object[] addArgs = new Object[] {reqXml};

        //指定getInfo方法返回值数据类型的class对象
        Class[] classs = new Class[] {String.class};

        //指定wsdl文件的命名空间及要调用的方法，命名空间为通过浏览器打开wsdl文件，看到的元素targetNamespace的属性值
        QName qname = new QName("http://facade.service.query.credit.picc.com/","getUniCredit");

        //调用getInfo方法并输出该方法的返回值
        //有三个参数：1.是QName对象，表示要调用的方法名  2.方法的参数值，参数类型是Object[] 3.返回值class对象，参数类型是Class[]
        String result = (String) serviceClient.invokeBlocking(qname, addArgs, classs)[0];

        System.out.println(result);

    }

    public static String bean2Json(Object object) {
        if (object == null) {
            return null;
        }
        Gson gson = new Gson();
        return gson.toJson(object);
    }

    public static RequestSearchReportVO create() {
        RequestSearchReportVO requestvo=new RequestSearchReportVO();
        requestvo.setName("李小文");
        requestvo.setIdentityNo("362429197007160913");
        requestvo.setRiskcode("BBR");
        requestvo.setSourcecode("00001");
        requestvo.setSourcepassword("BBR00001");
        requestvo.setOperatorcode("8899166");
        requestvo.setOperatorname("张三");
        requestvo.setComcode("10000002");
        requestvo.setComname("北京分公司");
        requestvo.setIdcardurl("");
        requestvo.setAuthorizeurl("");
        requestvo.setImagebakurl1("");
        requestvo.setImagebakurl2("");
        requestvo.setImagebakurl3("");
        requestvo.setExtend1("");
        requestvo.setExtend2("");
        requestvo.setExtend3("");
        requestvo.setExtend4("");
        requestvo.setExtend5("");

        return requestvo;
    }


    static class RequestSearchReportVO {
        private String name;    //姓名
        private String identityNo;     //身份证号
        private String riskcode;             //险种代码
        private String sourcecode;     //请求来源码
        private String sourcepassword;   //请求来源密码
        private String operatorname;     //查询人名称
        private String operatorcode;     //查询人代码
        private String comname;       //机构名称
        private String comcode;		  //机构代码
        private String idcardurl; 	   //身份证影像地址
        private String authorizeurl;     //授权书影像地址
        private String imagebakurl1;      //影像备用地址1
        private String imagebakurl2;    //影像备用地址2
        private String imagebakurl3;		//影像备用地址3
        private String extend1;			//备用字段1
        private String extend2; 		//备用字段2
        private String extend3;			//备用字段3
        private String extend4;			//备用字段4
        private String extend5;			//备用字段5

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIdentityNo() {
            return identityNo;
        }

        public void setIdentityNo(String identityNo) {
            this.identityNo = identityNo;
        }

        public String getRiskcode() {
            return riskcode;
        }

        public void setRiskcode(String riskcode) {
            this.riskcode = riskcode;
        }

        public String getSourcecode() {
            return sourcecode;
        }

        public void setSourcecode(String sourcecode) {
            this.sourcecode = sourcecode;
        }

        public String getSourcepassword() {
            return sourcepassword;
        }

        public void setSourcepassword(String sourcepassword) {
            this.sourcepassword = sourcepassword;
        }

        public String getOperatorname() {
            return operatorname;
        }

        public void setOperatorname(String operatorname) {
            this.operatorname = operatorname;
        }

        public String getOperatorcode() {
            return operatorcode;
        }

        public void setOperatorcode(String operatorcode) {
            this.operatorcode = operatorcode;
        }

        public String getComname() {
            return comname;
        }

        public void setComname(String comname) {
            this.comname = comname;
        }

        public String getComcode() {
            return comcode;
        }

        public void setComcode(String comcode) {
            this.comcode = comcode;
        }

        public String getIdcardurl() {
            return idcardurl;
        }

        public void setIdcardurl(String idcardurl) {
            this.idcardurl = idcardurl;
        }

        public String getAuthorizeurl() {
            return authorizeurl;
        }

        public void setAuthorizeurl(String authorizeurl) {
            this.authorizeurl = authorizeurl;
        }

        public String getImagebakurl1() {
            return imagebakurl1;
        }

        public void setImagebakurl1(String imagebakurl1) {
            this.imagebakurl1 = imagebakurl1;
        }

        public String getImagebakurl2() {
            return imagebakurl2;
        }

        public void setImagebakurl2(String imagebakurl2) {
            this.imagebakurl2 = imagebakurl2;
        }

        public String getImagebakurl3() {
            return imagebakurl3;
        }

        public void setImagebakurl3(String imagebakurl3) {
            this.imagebakurl3 = imagebakurl3;
        }

        public String getExtend1() {
            return extend1;
        }

        public void setExtend1(String extend1) {
            this.extend1 = extend1;
        }

        public String getExtend2() {
            return extend2;
        }

        public void setExtend2(String extend2) {
            this.extend2 = extend2;
        }

        public String getExtend3() {
            return extend3;
        }

        public void setExtend3(String extend3) {
            this.extend3 = extend3;
        }

        public String getExtend4() {
            return extend4;
        }

        public void setExtend4(String extend4) {
            this.extend4 = extend4;
        }

        public String getExtend5() {
            return extend5;
        }

        public void setExtend5(String extend5) {
            this.extend5 = extend5;
        }
    }

}
