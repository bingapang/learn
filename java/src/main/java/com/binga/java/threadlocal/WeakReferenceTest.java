package com.binga.java.threadlocal;

import java.lang.ref.WeakReference;

/**
 * @Description: 弱引用测试
 *               -Xms40m -Xmx40m -Xmn20m -XX:SurvivorRatio=8 -XX:+PrintGCDetails
 * @Author: binga
 * @Date: 2020/9/17 18:08
 * @Blog: https://blog.csdn.net/pang5356
 */
public class WeakReferenceTest {

    public static final int _1MB = 1024 * 1024;

    public static void main(String[] args) {
        CustomWeakReference customWeakReference =new  CustomWeakReference(new Key(), new Value());
        System.gc();
    }

    static class CustomWeakReference extends WeakReference<Key> {
        private byte[] data = new byte[4 * _1MB];
        private Value value;

        public CustomWeakReference(Key key) {
            super(key);
        }

        public CustomWeakReference(Key key, Value value) {
            super(key);
            this.value = value;
        }
    }

    static class Key {
        private byte[] data = new byte[4 * _1MB];
    }

    static class Value {
        private byte[] data = new byte[4 * _1MB];
    }
}
