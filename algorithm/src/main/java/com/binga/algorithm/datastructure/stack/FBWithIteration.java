package com.binga.algorithm.datastructure.stack;

/**
 * @Description: 使用迭代实现斐波那契数列，斐波那契数列如下
 *                0 1 1 2  3 5 8 13 21 ...
 *                每一个值都是前两个值的和
 * @Author: binga
 * @Date: 2020/12/7 16:41
 * @Blog: https://blog.csdn.net/pang5356
 */
public class FBWithIteration {


    public static void main(String[] args) {
        fb(30);
    }

    /**
     * 功能描述: 使用数组记录每一个位置的值，然后从头向后计算，通过将当前位置的前两个元素相加
     *          得到当前位置的元素值
     * @param: [i]
     * @return: void
     * @auther: binga
     * @date: 2020/12/7 16:54
     */
    public static void fb(int i) {


        int[] arr = new int[i];
        arr[0] = 0;
        arr[1] = 1;

        System.out.print(arr[0] + "  ");
        System.out.print(arr[1] + "  ");

        for (int j = 2; j < i; j++) {
            arr[j] = arr[j-1] + arr[j-2];
            System.out.print(arr[j] + "  ");
        }
    }
}
