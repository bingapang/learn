package com.binga.jvm.jvmtools;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: oom时生成dump文件测试
 *               -Xms10m -Xmx10m -XX:+PrintGCDetails -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=jvm.dump
 * @Author: binga
 * @Date: 2020/9/14 18:13
 * @Blog: https://blog.csdn.net/pang5356
 */
public class OOMDumpTest {

    public static void main(String[] args) {
        List<User> list = new ArrayList<User>();
        while(true) {
            list.add(new User());
        }
    }

    static class User {

    }
}
