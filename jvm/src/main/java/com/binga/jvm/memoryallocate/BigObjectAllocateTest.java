package com.binga.jvm.memoryallocate;

/**
 * @Description: 大对象直接进入老年代
 *                  -verbose:gc -Xms20m -Xmx20m -Xmn10m -XX:+PrintGCDetails -XX:SurvivorRatio=8
 *                  -XX:PretenureSizeThreshold=3145728 -XX:+UseConcMarkSweepGC
 * @Author: binga
 * @Date: 2020/9/8 20:22
 * @Blog: https://blog.csdn.net/pang5356
 */
public class BigObjectAllocateTest {

    public static final int _1M = 1024 * 1024;

    public static void main(String[] args) {
        byte[] a1;
        // 大对象直接在老年代分配
        a1 = new byte[4 * _1M];
    }
}
