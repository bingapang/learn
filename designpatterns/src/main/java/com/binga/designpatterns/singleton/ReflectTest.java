package com.binga.designpatterns.singleton;

import java.lang.reflect.Constructor;

/**
 * @Description: 使用反射验证各个单例实现能否保证单例
 * @Author: binga
 * @Date: 2020/8/20 17:29
 * @Blog: https://blog.csdn.net/pang5356
 */
public class ReflectTest {

    public static void main(String[] args) throws Exception {
//        // 懒汉模式反射测试
//        LazySingleton lazySingleton = LazySingleton.getInstance();
//        Constructor<LazySingleton> declaredConstructor = LazySingleton.class.getDeclaredConstructor();
//        declaredConstructor.setAccessible(true);
//        LazySingleton reflectInstance = declaredConstructor.newInstance();
//        System.out.println(lazySingleton == reflectInstance);

        // 饿汉模式反射测试
//        HungrySingleton hungrySingleton = HungrySingleton.getInstance();
//        Constructor<HungrySingleton> declaredConstructor = HungrySingleton.class.getDeclaredConstructor();
//        declaredConstructor.setAccessible(true);
//        HungrySingleton reflectInstance = declaredConstructor.newInstance();
//        System.out.println(hungrySingleton == reflectInstance);

//        // 枚举反射测试
//        EnumSingleton enumSingleton = EnumSingleton.INSTANCE;
//        Constructor<EnumSingleton> declaredConstructor = EnumSingleton.class.getDeclaredConstructor(String.class, int.class);
//        declaredConstructor.setAccessible(true);
//        EnumSingleton reflectInstance = declaredConstructor.newInstance("INSTANCE", 0);
//        System.out.println(enumSingleton == reflectInstance);

        // 静态内部类测试
        StaticInnerClassSingleton staticInnerClassSingleton = StaticInnerClassSingleton.getInstance();
        Constructor<StaticInnerClassSingleton> declaredConstructor = StaticInnerClassSingleton.class.getDeclaredConstructor();
        declaredConstructor.setAccessible(true);
        StaticInnerClassSingleton reflectInstance = declaredConstructor.newInstance();
        System.out.println(staticInnerClassSingleton == reflectInstance);

    }
}
