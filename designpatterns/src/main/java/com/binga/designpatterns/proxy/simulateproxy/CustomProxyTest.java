package com.binga.designpatterns.proxy.simulateproxy;

import com.binga.designpatterns.proxy.TargetImpl;
import com.binga.designpatterns.proxy.TargetInterface;

import java.io.Serializable;

/**
 * @Description: 自定义生成代理
 * @Author: binga
 * @Date: 2020/8/25 15:29
 * @Blog: https://blog.csdn.net/pang5356
 */
public class CustomProxyTest extends java.lang.Object implements Serializable {

    @Override
    public String toString() {
        return super.toString();
    }

    public static void main(String[] args) {
        MyHandler myHandler = new MyHandler(new TargetImpl());
        Object proxyInstance = MyProxy.getProxyInstance(CustomProxyTest.class.getClassLoader(),
                new Class[]{TargetInterface.class}, myHandler);
        TargetInterface proxyInstance1 = (TargetInterface) proxyInstance;
        ((TargetInterface) proxyInstance).doSomething();
    }
}
