package com.binga.java.threadlocal;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Description: 线程id实例
 * @Author: binga
 * @Date: 2020/9/17 17:16
 * @Blog: https://blog.csdn.net/pang5356
 */
public class ThreadId {

    public static void main(String[] args) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + " : " + ThreadId.getThreadId());
                System.out.println(Thread.currentThread().getName() + " : " + ThreadId.getThreadId());

            }
        };

        Thread t1 = new Thread(runnable, "t1");
        Thread t2 = new Thread(runnable, "t2");
        Thread t3 = new Thread(runnable, "t2");

        t1.start();
        t2.start();
        t3.start();
    }

    private static AtomicInteger nextId = new AtomicInteger(0);

    private static ThreadLocal<Integer> threadIds = ThreadLocal.withInitial(() -> {
        return nextId.getAndIncrement();
    });

    public static Integer getThreadId() {
        return threadIds.get();
    }
}
