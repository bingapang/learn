package com.binga.jvm.oom;

/**
 * @Description: 栈空间OutOfMemoryError
 *                -Xss2m
 * @Author: binga
 * @Date: 2020/8/26 23:04
 * @Blog: https://blog.csdn.net/pang5356
 */
public class StackOOMTest3 {

    public static void main(String[] args) {
        StackOOMTest3 stackOOMTest3 = new StackOOMTest3();
        stackOOMTest3.stackWithThread();
    }

    private void dontStop() {
        while (true) {

        }
    }

    public void stackWithThread() {
        while (true) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    dontStop();
                }
            }).start();
        }
    }
}
