package com.binga.jvm.classloader.customclasslaoder;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @Description: 自定义类加载器测试
 * @Author: binga
 * @Date: 2020/8/18 17:07
 * @Blog: https://blog.csdn.net/pang5356
 */
public class CustomClassLoaderTest {

    public static void main(String[] args) throws Exception {
        MyClassLoader myClassLoader = new MyClassLoader("F:\\test");
        Class<?> clazz = myClassLoader.loadClass("com.binga.jvm.classloader.User");
        Object o = clazz.newInstance();
        Method say = clazz.getDeclaredMethod("say");
        say.invoke(o);
    }
}
