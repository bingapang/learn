package com.binga.designpatterns.fatory.factorymethod;

/**
 * @Description: 薪酬计算器，通过给定的人员代码，计算人员的指定薪酬项
 *               计算完成后保存薪酬项
 * @Author: binga
 * @Date: 2020/8/20 09:51
 * @Blog: https://blog.csdn.net/pang5356
 */
public interface SalaryCalculator {
    public void calculate(String userCode);
}
