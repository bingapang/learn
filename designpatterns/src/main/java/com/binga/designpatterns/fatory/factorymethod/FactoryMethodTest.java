package com.binga.designpatterns.fatory.factorymethod;

/**
 * @Description: 工厂方法测试类
 * @Author: binga
 * @Date: 2020/8/20 10:22
 * @Blog: https://blog.csdn.net/pang5356
 */
public class FactoryMethodTest {

    public static void main(String[] args) {
        String userCode = "11000000";

        // 计算基本工资
        BaseSalaryCalculatorFactory baseSalaryCalculatorFactory = new BaseSalaryCalculatorFactory();
        SalaryCalculator calculator = baseSalaryCalculatorFactory.calculator();
        calculator.calculate(userCode);

        // 计算绩效工资
        PerformanceSalaryCalculatorFactory performanceSalaryCalculatorFactory = new PerformanceSalaryCalculatorFactory();
        SalaryCalculator calculator1 = performanceSalaryCalculatorFactory.calculator();
        calculator1.calculate(userCode);
    }
}
