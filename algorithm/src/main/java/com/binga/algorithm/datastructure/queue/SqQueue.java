package com.binga.algorithm.datastructure.queue;

/**
 * @Description: 使用顺序存储结构实现队列，即数组实现队列。
 *               使用数据存储我们可以将元素从数组下标为0的位置存储，
 *               新入队的元素一次追加，但是这就会有一个问题，当我们需要从
 *               队列删除一个元素时，将下标为0的元素删除，然后后续的元素
 *               依次往前移动，这样的效率是比较低的，我们通过使用两个指针
 *               head 和 tail两个指针来标识队列头和尾的下标。也就是循环队列，
 *               需要处理的问题就是当tail达到数组的最大下标时如何处理，那就是
 *               将tail再0这个下标开始，而head同理也是。
 *               那么还有一个问题就是如何判断队列为空还是已经满了，这里有两种
 *               方法：
 *               1>使用一个size变量记录队列中的元素个数，当元素个数等于数组
 *                 长度，则队列已满
 *               2>通过预留一个空闲下标，当满足以下条件时则队列已满
 *                  （tail + 1） % 数组长度 = head
 *               那么判断队列为空则可以通过如下判断
 *                  head == tail
 *
 *               而队列中元素个数的计算公式为
 *                  (tail - head + 数组长度 ) % 数组长度
 *
 * @Author: binga
 * @Date: 2020/12/9 14:39
 * @Blog: https://blog.csdn.net/pang5356
 */
public class SqQueue implements Queue {

    // 存储队列元素的数组
    private Object[] table;

    // 头指针
    private int head;

    // 尾指针
    private int tail;

    public SqQueue(int queueSize) {
        // 因为要预留一个空闲为，所以数组的长度为queueSize + 1
        this.table = new Object[queueSize + 1];
        this.head = 0;
        this.tail = 0;
    }

    public int length() {
        return (this.tail - this.head + table.length) % table.length;
    }

    public boolean isEmpty() {
        return this.head == this.tail;
    }

    public void enqueue(Object elem) {
        // 首先判断队列是否已经满了
        if (((tail + 1) % table.length) == head) throw new RuntimeException("queue is full");

        table[tail] = elem;
        tail = (tail + 1) % table.length;
    }

    public Object dequeue() {
        if (head == tail) throw new RuntimeException("queue is empty");
        Object elem = table[head];
        head = (head + 1) % table.length;
        return elem;
    }
}
