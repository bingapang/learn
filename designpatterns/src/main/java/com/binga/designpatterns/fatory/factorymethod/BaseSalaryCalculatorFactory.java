package com.binga.designpatterns.fatory.factorymethod;

/**
 * @Description: 生产基本薪酬计算器的工厂
 * @Author: binga
 * @Date: 2020/8/20 10:17
 * @Blog: https://blog.csdn.net/pang5356
 */
public class BaseSalaryCalculatorFactory implements CalculatorFactory {

    public SalaryCalculator calculator() {
        // 创建计算器
        BaseSalaryCalculator baseSalaryCalculator = new BaseSalaryCalculator();
        // 初始化配置等
        System.out.println("baseSalaryCalculator config");
        return baseSalaryCalculator;
    }
}
