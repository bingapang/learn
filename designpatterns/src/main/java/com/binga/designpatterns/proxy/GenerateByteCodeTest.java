package com.binga.designpatterns.proxy;

import sun.misc.ProxyGenerator;

import java.io.*;

/**
 * @Description: 输出字节码
 * @Author: binga
 * @Date: 2020/8/25 10:34
 * @Blog: https://blog.csdn.net/pang5356
 */
public class GenerateByteCodeTest {

    public static final String CLAZZ_PATH = "F:\\test\\";

    public static void main(String[] args) {
        TargetProxy proxy = new TargetProxy(new TargetImpl());
        TargetInterface proxyInstance = proxy.getProxyInstance();
        proxyInstance.doSomething();

        String clazzName = proxyInstance.getClass().getName();

        byte[] classByteCode = ProxyGenerator.generateProxyClass(clazzName,
                new Class[]{TargetInterface.class});

        String name = clazzName.substring(clazzName.lastIndexOf(".") + 1);
        File file = new File(CLAZZ_PATH, name + ".class");

        try (FileOutputStream fout = new FileOutputStream(file);) {
            fout.write(classByteCode);
            fout.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
