package com.binga.designpatterns.fatory.abstractfactory;

/**
 * @Description: 抽象工厂测试
 * @Author: binga
 * @Date: 2020/8/20 11:11
 * @Blog: https://blog.csdn.net/pang5356
 */
public class AbstractFactoryTest {

    public static void main(String[] args) {

        // 买nike衣服
        NikeClothingFactory nikeClothingFactory = new NikeClothingFactory();
        Shoes shoes = nikeClothingFactory.productShoes();
        TShirt tShirt = nikeClothingFactory.productTShirt();
        shoes.disPlay();
        tShirt.show();
    }
}
