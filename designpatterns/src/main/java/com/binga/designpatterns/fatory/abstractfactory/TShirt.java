package com.binga.designpatterns.fatory.abstractfactory;

/**
 * @Description: T-shirt
 * @Author: binga
 * @Date: 2020/8/20 10:52
 * @Blog: https://blog.csdn.net/pang5356
 */
public interface TShirt {

    void show();
}
