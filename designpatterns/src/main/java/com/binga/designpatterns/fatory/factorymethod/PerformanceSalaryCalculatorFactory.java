package com.binga.designpatterns.fatory.factorymethod;

/**
 * @Description: 生产计算绩效的计算器工厂
 * @Author: binga
 * @Date: 2020/8/20 10:20
 * @Blog: https://blog.csdn.net/pang5356
 */
public class PerformanceSalaryCalculatorFactory implements CalculatorFactory {
    public SalaryCalculator calculator() {

        PerformanceSalaryCalculator performanceSalaryCalculator = new PerformanceSalaryCalculator();
        System.out.println("config performanceSalaryCalculator");
        return performanceSalaryCalculator;
    }
}
