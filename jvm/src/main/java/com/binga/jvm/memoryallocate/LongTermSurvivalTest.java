package com.binga.jvm.memoryallocate;

/**
 * @Description: 长期存活的对象进入老年代
 *               -Xms60m -Xmx60m -Xmn20m -XX:+PrintGCDetails -XX:SurvivorRatio=8 -verbose:gc -XX:PretenureSizeThreshold=20971520 -XX:+UseConcMarkSweepGC -XX:MaxTenuringThreshold=1
 * @Author: binga
 * @Date: 2020/9/8 20:35
 * @Blog: https://blog.csdn.net/pang5356
 */
public class LongTermSurvivalTest {

    public static final int _1M = 1024 * 1024;

    public static void main(String[] args) {
        byte[] a1, a2, a3;
        // 2.88m
        a1 = new byte[_1M / 4];

        a2 = new byte[8 * _1M];
        // 在位a3分配内存空间时，先进行Minor GC,此时a2由于放不下，所以会直接进入老年代
        a3 = new byte[8 * _1M];
        a3 = null;
        // 第二次进行Minor GC,该次GC完后，对于参数指定-XX:MaxTenuringThreshold=1的情景
        // 所以此时a1及程序对象都满足>1 此时，对象都进入老年代，所以第二次Minor GC完成后
        // [GC (Allocation Failure) [ParNew: 9118K->0K(18432K), 0.0033386 secs] 年轻代为0了。
        a3 = new byte[8 * _1M];
    }
}
