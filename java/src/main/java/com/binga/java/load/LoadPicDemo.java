package com.binga.java.load;

import java.io.*;
import java.util.Arrays;

/**
 * @Description: 加载图片
 * @Author: binga
 * @Date: 2020/11/18 10:17
 * @Blog: https://blog.csdn.net/pang5356
 */
public class LoadPicDemo {

    public static void main(String[] args) throws IOException {

        // 读取
        InputStream in = LoadPicDemo.class.getResourceAsStream("/pic.png");
        BufferedInputStream bIn = new BufferedInputStream(in);
        // 先开辟1MB空间
        byte[] data = new byte[1024 * 1024];

        int temp;
        int position = 0;
        while((temp = bIn.read()) != -1) {
            data[position++] = (byte) temp;
        }
        byte[] pic = Arrays.copyOf(data, position);
        bIn.close();

        // 写出

        BufferedOutputStream bout = new BufferedOutputStream(new FileOutputStream("C:\\Users\\Administrator\\Desktop\\piccopy.png"));
        bout.write(pic);
        bout.flush();
        bout.close();
    }
}
