package com.binga.jvm.classloader.customclasslaoder;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;

/**
 * @Description: 自定义类加载器，通过继承ClassLoader实现
 * @Author: binga
 * @Date: 2020/8/18 16:57
 * @Blog: https://blog.csdn.net/pang5356
 */
public class MyClassLoader extends ClassLoader {

    private final String path;

    public MyClassLoader(String path) {
        this.path = path;
    }

    /**
     * 功能描述: 重载CLassLoader的findClass方法，在该方法中通过指定的path，通过
     *          指定的类的全限定名称转换为路径从而读取class文件，然后通defineClass方法
     *          加载类并返回指定类的Class对象。
     * @param: [name]
     * @return: java.lang.Class<?>
     * @auther: binga
     * @date: 2020/8/18 17:03
     */
    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        byte[] byteCode = getByteArrayFromClassName(name);
        return defineClass(name, byteCode, 0, byteCode.length);
    }

    /**
     * 功能描述: 加载class文件返回字节数组
     * @param: [name]
     * @return: byte[]
     * @auther: binga
     * @date: 2020/8/18 17:05
     */
    private byte[] getByteArrayFromClassName(String name) throws ClassNotFoundException {
        String classPath = convertNameToPath(name);
        byte[] data = null;

        int off = 0;
        int length;

        try(BufferedInputStream bufferedInputStream =
                    new BufferedInputStream(new FileInputStream(classPath))) {
            data = new byte[bufferedInputStream.available()];
            while ((length = bufferedInputStream.read(data, off, data.length - off)) > 0) {
                off += length;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ClassNotFoundException();
        }
        return data;
    }

    /**
     * 功能描述: 类全限定名称转换为文件路径
     * @param: [name]
     * @return: java.lang.String
     * @auther: binga
     * @date: 2020/8/18 17:06
     */
    private String convertNameToPath(String name) {
        String classPath = name.replace(".", File.separator);
        classPath = path + File.separator + classPath + ".class";
        return classPath;
    }
}
