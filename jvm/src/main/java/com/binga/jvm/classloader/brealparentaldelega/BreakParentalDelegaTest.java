package com.binga.jvm.classloader.brealparentaldelega;

import java.lang.reflect.Method;

/**
 * @Description: 打破双亲委派测试
 * @Author: binga
 * @Date: 2020/8/18 17:49
 * @Blog: https://blog.csdn.net/pang5356
 */
public class BreakParentalDelegaTest {

    public static void main(String[] args) throws Exception {
        BreakParentalDelegaClassLoader classLoader = new BreakParentalDelegaClassLoader("F:\\test");
        Class<?> userClass = classLoader.loadClass("com.binga.jvm.classloader.User", false);
        Object user = userClass.newInstance();

        Method say = userClass.getDeclaredMethod("say", null);
        say.invoke(user, null);
    }
}
