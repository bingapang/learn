package com.binga.java.juc.blockingqueue;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Description: 数组实现堵塞队列
 * @Author: binga
 * @Date: 2020/11/5 14:41
 * @Blog: https://blog.csdn.net/pang5356
 */
public class CustomArrayBlockingQueue<T> implements CustomBlockingQueue<T> {

    // 元素数组，存放元素
    private  Object[] items;
    // 记录当前队列中的元素个数
    private volatile int count;

    // 每次存放元素的下标
    private int putIndex;
    // 每次获取时元素下标
    private int takeIndex;

    // 全局锁，放入或者存储元素时均需加锁
    private ReentrantLock lock;

    // 当队列为空，将所有获取元素线程添加至该对象的条件队列中
    private Condition notEmpty;

    // 当队列满时，将所有添加元素线程添加至该队列的条件队列中
    private Condition notFull;

    public CustomArrayBlockingQueue(int capacity) {
        // 默认非公平
        this(capacity, false);
    }

    public CustomArrayBlockingQueue(int capacity, boolean fair) {
        if (capacity <= 0) {
            throw new IllegalArgumentException("capacity can not be 0 or negative");
        }

        this.items = new Object[capacity];
        this.lock = new ReentrantLock(fair);
        this.notEmpty = this.lock.newCondition();
        this.notFull = this.lock.newCondition();
    }

    @Override
    public T take() {
        // 获取元素先获取锁
        ReentrantLock lock = this.lock;
        lock.lock();
        T t = null;

        try {
            // 没有元素啊，去notEmpty里面排队去
            while (0 == this.count) {
                notEmpty.await();
            }

            // 满足条件了，嘿嘿，获取元素
            t = dequeue();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 老铁。别忘啦解锁
            lock.unlock();
        }

        return t;
    }

    @Override
    public void put(T t) {
        ReentrantLock lock = this.lock;

        lock.lock();
        int length = this.items.length;

        try {
            // 条件满足，队列满了还放毛，排队去
            while(this.count == length) {
               notFull.await();
            }

            // 被唤醒了，队列还没满，方
            enqueue(t);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            // 记得解锁老铁
            lock.unlock();
        }
    }

    public int getCount() {
        return this.count;
    }
    public Object[] elements() {
        return this.items;
    }
    // 定义入队和出队的两个方法
    public void enqueue(T t) {
        if (null == t) {
            throw new NullPointerException("element t can not be null");
        }

        Object[] elements = this.items;
        // 1.方元素
        elements[putIndex] = t;
        // 2. 判断指针是否查过长度了
        if (++putIndex == elements.length) {
            putIndex = 0;
        }

        // 3.数量加一
        this.count++;

        // 4.既然放入元素了，可能获取元素的线程还在notEmpty中阻塞
        // 着嘞，所以唤醒这些线程
        // 唤醒一个比较靠谱，因为就放入了一个
        notEmpty.signal();
    }

    public T dequeue() {
        // 1.获取元素啊
        Object[] elements = this.items;
        Object element = elements[takeIndex];
        // 切断引用
        elements[takeIndex] = null;
        if (++takeIndex == elements.length) {
            takeIndex = 0;
        }

        // 2.数量减一
        this.count--;

        // 3.获取了一个元素，唤醒那些存放元素的线程，
        // 可能都还在notFull条件队列中阻塞着嘞
        notFull.signal();
        return (T)element;
    }
}
