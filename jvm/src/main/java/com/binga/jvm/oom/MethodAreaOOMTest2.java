package com.binga.jvm.oom;

import java.util.HashSet;
import java.util.Set;

/**
 * @Description: 方法区溢出
 *              -Xms64m -Xmx64m -XX:MetaspaceSize=8m -XX:MaxMetaspaceSize=8m -XX:-UseGCOverheadLimit
 * @Author: binga
 * @Date: 2020/8/26 15:19
 * @Blog: https://blog.csdn.net/pang5356
 */
public class MethodAreaOOMTest2 {

    public static void main(String[] args) throws InterruptedException {
        Set<String> strs = new HashSet<>();
        int count = 0;
        while(true) {
            strs.add(String.valueOf(count++).intern());
        }
    }
}
