package com.binga.java.datastructure;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Description: LockDemo
 * @Author: binga
 * @Date: 2020/10/30 16:53
 * @Blog: https://blog.csdn.net/pang5356
 */
public class LockDemo {

    private Lock lock = new ReentrantLock();
    private Condition condition = lock.newCondition();


    public void block() {

        try {
            lock.lock();
            while(!check()) {
                condition.await();
            }
            // doSomething
         } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    private void nonBlock() {
        try {
            lock.lock();
            // doSomething
            if (check()) {
               condition.signalAll();
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
        lock.unlock();
    }

    private boolean check() {
        // 检查条件是否继续执行
        return false;
    }
}
