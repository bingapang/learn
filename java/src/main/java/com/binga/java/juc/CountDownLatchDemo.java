package com.binga.java.juc;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;

/**
 * @Description: CountDownLatchDemo
 * @Author: binga
 * @Date: 2020/11/2 09:43
 * @Blog: https://blog.csdn.net/pang5356
 */
public class CountDownLatchDemo {

    public static void main(String[] args) throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(3);

        Thread t1 = new Thread(new Task(countDownLatch));
        Thread t2 = new Thread(new Task(countDownLatch));
        Thread t3 = new Thread(new Task(countDownLatch));


        t1.start();
        t2.start();
        t3.start();

        countDownLatch.await();
        System.out.println("3个任务完成");

    }

    static class Task implements Runnable {

        private final CountDownLatch countDownLatch;

        Task(CountDownLatch countDownLatch) {
            this.countDownLatch = countDownLatch;
        }


        @Override
        public void run() {
            System.out.println("a task finished");
            countDownLatch.countDown();
        }
    }
}
