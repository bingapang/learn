package com.binga.jvm.escapeanalysis;

/**
 * @Description: 逃逸分析之栈上分配
 *              VM args: -Xms64m -Xmx64m -XX:+PrintGC -XX:-DoEscapeAnalysis
 * @Author: binga
 * @Date: 2020/8/28 16:45
 * @Blog: https://blog.csdn.net/pang5356
 */
public class StackAllocationTest {

    public static void main(String[] args) {
        while (true) {
            User user = new User();
        }
    }
}

class User {

    private String name;
    private int age;
    private char sex;
}
