package com.binga.java.redis;

import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.client.RedisClient;
import org.redisson.config.Config;

/**
 * @Description: Redisson demno
 * @Author: binga
 * @Date: 2020/11/11 17:20
 * @Blog: https://blog.csdn.net/pang5356
 */
public class RedissonDemo {

    private static Redisson redisson;

    static {
        Config config = new Config();
        config.useSingleServer().setAddress("redis://localhost:6379").setDatabase(0);
        config.useSingleServer().setPassword("xexd");
        redisson = (Redisson) Redisson.create(config);
    }

    public static void main(String[] args) throws InterruptedException {

        RLock lock = redisson.getLock("product");

        lock.lock();

        try {
            while(true) {
                Thread.sleep(1000);
                System.out.println("message");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }


}
