package com.binga.algorithm.datastructure.stack;

/**
 * @Description: 使用递归计算斐波那契数列
 * @Author: binga
 * @Date: 2020/12/7 16:56
 * @Blog: https://blog.csdn.net/pang5356
 */
public class FBWithRecursion {

    public static void main(String[] args) {
        for (int i = 0; i < 15; i++) {
            System.out.print(fb(i) + "  ");
        }
    }

    public static int fb(int i) {
        // 递归出口，
        if (i < 2) {
            return  i == 0 ? 0 : 1;
        }
        // 当前值等于前两个值的和
        return fb(i -1) + fb(i-2);
    }
}
