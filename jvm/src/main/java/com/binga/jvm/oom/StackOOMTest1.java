package com.binga.jvm.oom;

/**
 * @Description: 栈内存溢出
 *               -Xmx64m -Xms64m -Xss128k -XX:+PrintGC -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=C:\Users\Administrator\Desktop\
 * @Author: binga
 * @Date: 2020/8/26 14:34
 * @Blog: https://blog.csdn.net/pang5356
 */
public class StackOOMTest1 {

    public static int count = 0;

    public static void main(String[] args) {
        try {
            recurse();
        } catch (Throwable e) {
            System.out.println(count);
            e.printStackTrace();

        }
    }

    public static void recurse() {
        count++;
        recurse();
    }
}
