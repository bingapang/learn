package com.binga.algorithm.datastructure.queue;

/**
 * @Description: 链表存储结构实现队列测试类
 * @Author: binga
 * @Date: 2020/12/9 16:07
 * @Blog: https://blog.csdn.net/pang5356
 */
public class LinkQueueTest {

    public static void main(String[] args) {

        Queue queue = new LinkQueue();

        // 先放15个数
        for (int i = 0; i < 15; i++) {
            System.out.println(i);
            queue.enqueue(i);
        }

        System.out.println(queue.length());

        try {
            queue.enqueue(100);
        } catch (Exception e) {
            e.printStackTrace();
        }

        while(!queue.isEmpty()) {
            System.out.println(queue.dequeue());
        }

        System.out.println(queue.length());
    }
}
