package com.binga.java.datastructure;

import java.util.*;

/**
 * @Description: List集合展示
 * @Author: binga
 * @Date: 2020/9/24 17:45
 * @Blog: https://blog.csdn.net/pang5356
 */
public class ListTest {

    private static Object lock = new Object();

    private static Object value = null;


    public static void main(String[] args) throws InterruptedException {
//        Integer[] ints = new Integer[6];
//        ints[4] = 10;
//        ints[5] = 20;
//        forEach(ints);
//        List<Integer> list = new ArrayList<>();
//        list.add(1);
//        list.add(2);
//        list.add(3);
//        list.add(4);
//        list.toArray(ints);
//        forEach(ints);
//
//        // LinkedList
//        LinkedList<Integer> linkedList = new LinkedList<>();
//
//        // Vector
//        Vector<Integer> vector = new Vector<>();
//
//        int a = 1;
//        a = a << 30;
//        System.out.println(a);
//
////        ArrayDeque<Integer> arrayDeque = new ArrayDeque<>();
////        arrayDeque.removeFirst();
//
//        System.out.println((0 - 1)& 20);

        new Thread(new Runnable() {
            @Override
            public void run() {
                method1();
            }
        }).start();

        Thread.sleep(500);

        new Thread(new Runnable() {
            @Override
            public void run() {
                method2();
            }
        }).start();

        Thread.sleep(Integer.MAX_VALUE);
    }

    public static void method1() {
        Object o = value;
        synchronized (lock) {
            try {
                lock.wait();
                System.out.println(Thread.currentThread().getName() + o);
                o = value;
                System.out.println(Thread.currentThread().getName() + o);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void method2() {
        synchronized (lock) {
            value = new Object();
            System.out.println(Thread.currentThread().getName() + "init value");
            lock.notifyAll();
        }
    }

    public static void forEach(Integer[] arr) {
        System.out.print("{");
        for (int i = 0; i < arr.length; i++) {
            if (i == arr.length -1)
                System.out.print(arr[i]);
            else
                System.out.print(arr[i] + ",");
        }
        System.out.println("}");
    }
}
