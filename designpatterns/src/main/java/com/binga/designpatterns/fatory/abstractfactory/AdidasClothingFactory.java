package com.binga.designpatterns.fatory.abstractfactory;

/**
 * @Description: adidas 工厂
 * @Author: binga
 * @Date: 2020/8/20 11:10
 * @Blog: https://blog.csdn.net/pang5356
 */
public class AdidasClothingFactory implements ClothingFactory {
    public TShirt productTShirt() {
        return new AdidasTShirt();
    }

    public Shoes productShoes() {
        return new AdidasShoes();
    }
}
