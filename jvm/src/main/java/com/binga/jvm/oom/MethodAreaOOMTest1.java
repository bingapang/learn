package com.binga.jvm.oom;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @Description: 方法区内存溢出测试
 *                -Xms512m -Xmx512m -XX:MetaspaceSize=64m -XX:MaxMetaspaceSize=64m
 * @Author: binga
 * @Date: 2020/8/26 14:42
 * @Blog: https://blog.csdn.net/pang5356
 */
public class MethodAreaOOMTest1 {

    public static void main(String[] args) {
        try {
            while (true) {
                Thread.sleep(5);
                Enhancer enhancer = new Enhancer();
                enhancer.setSuperclass(Simple.class);
                enhancer.setUseCache(false);
                enhancer.setCallback(new MethodInterceptor() {
                    @Override
                    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {

                        return methodProxy.invokeSuper(o, objects);
                    }
                });
                enhancer.create();
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}

class Simple {
    public void doSomething() {
        System.out.println("hello");
    }
}
