package com.binga.designpatterns.proxy;

/**
 * @Description: 代理目标接口
 * @Author: binga
 * @Date: 2020/8/25 10:19
 * @Blog: https://blog.csdn.net/pang5356
 */
public interface TargetInterface {
    void doSomething();
}
