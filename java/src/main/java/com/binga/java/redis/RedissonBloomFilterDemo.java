package com.binga.java.redis;

import org.redisson.Redisson;
import org.redisson.api.RBloomFilter;
import org.redisson.config.Config;
import java.util.Scanner;

/**
 * @Description: TODO
 * @Author: binga
 * @Date: 2020/11/13 16:50
 * @Blog: https://blog.csdn.net/pang5356
 */
public class RedissonBloomFilterDemo {

    private static Redisson redisson;

    static {
        Config config = new Config();
        config.useSingleServer().setAddress("redis://localhost:6379").setDatabase(0);
        config.useSingleServer().setPassword("xexd");
        redisson = (Redisson) Redisson.create(config);
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        RBloomFilter<Object> bloomFilter = redisson.getBloomFilter("user:filter");
        bloomFilter.tryInit(100000000, 0.001);

        for (int i = 0; i < 10; i++) {
            String userId = "user:100" + i;
            System.out.println(userId);
            bloomFilter.add(userId);
        }

        System.out.println(bloomFilter.count());

        System.out.println(bloomFilter.contains("user:1001"));
        System.out.println(bloomFilter.contains("user:10011"));

        String userId = null;

        while (true) {
            System.out.println("enter user id ");
            userId = scanner.nextLine();
            boolean contains = bloomFilter.contains("user:" + userId);
            if (contains) {
                System.out.println("用户user:" + userId + "存在");
            } else {
                System.out.println("用户user:" + userId + "不存在");
            }
        }


    }
}
