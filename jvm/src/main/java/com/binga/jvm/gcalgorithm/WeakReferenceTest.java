package com.binga.jvm.gcalgorithm;

import java.lang.ref.WeakReference;

/**
 * @Description: 弱引用测试
 *              -Xms20m -Xmx20m -Xmn10m -verbose:gc -XX:+PrintGCDetails -XX:PretenureSizeThreshold=20971520 -XX:+UseConcMarkSweepGC
 * @Author: binga
 * @Date: 2020/9/9 21:37
 * @Blog: https://blog.csdn.net/pang5356
 */
public class WeakReferenceTest {

    public static final int _1MB = 1024 * 1024;

    public static void main(String[] args) {
        WeakReference<User> user1 = new WeakReference<>(new User());
        WeakReference<User> user2 = new WeakReference<>(new User());
    }

    static class User {
        private byte[] m = new byte[4 * _1MB];
    }
}
