package com.binga.algorithm.datastructure.lineartable;

/**
 * @Description: 链式存储结构实现线性表
 *               链式存储结构实现线性表，由于各个元素之间不像数据那样，通过数组下标
 *               可以确定各个元素之前的位置关系，所有出去需要存储数据元素的内容，还需要
 *               新增各个数据元素之前的关系，即指针。 数据元素和指针共同组成链表中的节点。
 *               这里实现的是一个单向的链表，数据添加时使用尾插法，也就是数据在添加时从尾
 *               部添加。
 *
 *               同时，链表中维护一个头节点，该节点数据域不存贮数据，只是作为一个链表的指针，当线性表中不为
 *               空时，其维护的指针next指向线性表中的第一个元素。
 *               维护一个尾部指针，该指针指向线性表中的最后一个元素，当然当线性表为空时，满足如下条件
 *               head == tail
 *
 *               这里为了统一，链表中元素的顺序也是从0开始，范围为[0,size)
 * @Author: binga
 * @Date: 2020/12/3 17:27
 * @Blog: https://blog.csdn.net/pang5356
 */
public class LinkList implements List {

    // 链表的头节点，但不是线性表的第一个元素
    private Node head;

    // 链表的尾节点
    private Node tail;

    // 维护线性表中元素的个数，当然可以不维护，如果不维护，当index参数越界时需要遍历链表才可知，通过
    // 维护size可直接对是否越界进行判断
    private int size;

    public LinkList() {
        head = new Node();
        tail = head;
    }

    /**
     * 功能描述: 获取线性表中元素个数
     * @param: []
     * @return: int
     * @auther: binga
     * @date: 2020/12/3 17:41
     */
    public int length() {
        return size;
    }

    /**
     * 功能描述: 根据index获取指定的元素
     * @param: [index]
     * @return: java.lang.Object
     * @auther: binga
     * @date: 2020/12/3 17:43
     */
    public Object get(int index) {
        // 检验index合法性
        if (index < 0 || index >= size) throw new IndexOutOfBoundsException("index is not illegal");

        Node p = head;
        int i = 0;

        while (i <= index) {
            p = p.next;
            i++;
        }

        return p.elem;
    }

    /**
     * 功能描述: 向线性表中插入元素
     * @param: [index, elem]
     * @return: void
     * @auther: binga
     * @date: 2020/12/3 17:52
     */
    public void insert(int index, Object elem) {
        // 新增加元素，所以index的范围应该为[0,size]
        if (index < 0 || index > size) throw new IndexOutOfBoundsException("index is not illegal");

        // 创建节点，并将参数数据元素添加至新创建的节点
        Node p = new Node();
        p.elem = elem;

        // 添加至尾部，则直接追加至尾部
        if (index == size) {
            // 变更指针，先将原先的尾部节点的指针指向新加入的节点
            tail.next = p;
            // 然后将tail指针指向新加入的节点
            tail = p;
            size++;
            return;
        }

        // 不是尾部的话则需要从头部遍历找到插入的位置，但是获取到index的前置节点
        Node pre = head;
        int i = 0;

        while (i < index) {
            pre = pre.next;
            i++;
        }

        // 遍历完成后，此时p就是要插入位置index的前置节点
        // 接下修改指针关系
        // 先将新增节点的next指针指向原先在index位置的元素，也就是pre.next
        p.next = pre.next;
        // 然后将pre的next指针指向新增的节点
        pre.next = p;
        size++;
    }

    /**
     * 功能描述: 从线性表中删除指定序号index的元素
     *          需要将删除元素的后续节点追加到删除元素的前置节点之后
     * @param: [index]
     * @return: java.lang.Object
     * @auther: binga
     * @date: 2020/12/3 18:04
     */
    public Object delete(int index) {
        if (index < 0 || index >= size) throw new IndexOutOfBoundsException("index is not illegal");

        Node p = head;
        Node del = null;
        int i = 0;
        // 这里需要遍历到删除元素的前一个节点
        while (i < index) {
            p = p.next;
            i++;
        }

        // 此时p为删除节点的前一个节点
        // 先保存要删除的节点
        del = p.next;
        // 将要删除节点的前置节点的next指针指向要删除节点的后置节点
        p.next = p.next.next;

        Object elem = del.elem;
        // help gc
        del.next = null;
        del.elem = null;
        size--;
        return elem;
    }

    /**
     * Node类就是链表中的节点
     * Node中有两个域 elem就是存储数据元素的数据域，而next则是存储指向链表中
     * 下一个节点的指针。
     */

    static class Node {
        Object elem;
        Node next;
    }
}
