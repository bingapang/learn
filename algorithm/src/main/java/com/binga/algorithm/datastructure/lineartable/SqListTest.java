package com.binga.algorithm.datastructure.lineartable;

/**
 * @Description: SqList测试类
 * @Author: binga
 * @Date: 2020/12/3 15:26
 * @Blog: https://blog.csdn.net/pang5356
 */
public class SqListTest {

    public static void main(String[] args) {
        SqList sqList = new SqList();

        for (int i = 0; i < 20; i++) {
            System.out.println(i);
            sqList.insert(i, i);
        }

        //
        /**
         *  这里的删除 开始时  0 1 2 3 4 ... 19
         *  i = 0   删除后  1 2 3 4 5 ... 19
         *  i = 1   删除后  1 3 4 5 6 .... 19
         *  i = 2   删除后  1 3 5 6 7 ....19
         *
         *  依次类推，最后剩下
         *  i = 9 删除后 1 3 5 7 9 11 13 15 17 19
         */

        for (int i = 0; i < 10; i++) {
            sqList.delete(i);
        }

        for (int i = 0; i < sqList.length(); i++) {
            System.out.println(sqList.get(i));
        }
    }
}
