package com.binga.designpatterns.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @Description: 代理类
 * @Author: binga
 * @Date: 2020/8/25 10:16
 * @Blog: https://blog net/pang5356
 */
public class TargetProxy implements InvocationHandler {

    private TargetInterface targetInterface;

    public TargetProxy(TargetInterface targetInterface) {
        this.targetInterface = targetInterface;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object invoke = null;

        System.out.println(method.getName() + "前置处理");
        if (targetInterface != null)
            invoke  = method.invoke(targetInterface, args);
        System.out.println(method.getName() + "后置处理");
        return invoke;
    }

    public TargetInterface getProxyInstance() {
        Object o = Proxy.newProxyInstance(TargetProxy.class.getClassLoader(),
                targetInterface.getClass().getInterfaces(), this);
//        Object o = Proxy.newProxyInstance(TargetProxy.class.getClassLoader(),
//                new Class[] {TargetInterface.class}, this);
        return (TargetInterface) o;
    }
}
